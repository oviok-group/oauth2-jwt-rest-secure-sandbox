# User Management REST API   ![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg?branch=develop)

`My User` est non seulement une application de gestion des accès aux ressources sécurisées, mais également de gestion des utilisateurs et de leur rôle dans le système d'informations.
`My User` est un simple service Web RESTful écrit en **Java** et embarque **Spring** (_Spring Boot_, _Spring Security_, ...) et d'autres technologies non seulement pour l'intégration des différents composants applicatifs
mais également pour la sécurisation de ces composants. Il fournit principalement :
- un **Back-End**, permettant entre autres de :
	- _Sécuriser l'application et les différents échanges_ en proposant : 
		- _La Gestion de l'Authentification_ : qui permet de confirmer ou valider l'identité du client ou de l’utilisateur qui tente d’accéder au système d'informations. 
		- _La Gestion de l'Autorisation (protection des ressources)_ : permet d’octroyer l’accès au système d’informations (donc aux ressources) au client ou à l’utilisateur de ce dernier.
	- _Gérer la Connexion/Déconnexion à l'application_
	- _Gérer l'ajout de nouveaux utilisateurs_ dans le SI avec leur rôle 
	- _Mettre à jour les informations_ d'un utilisateur existant.
	- _Supprimer les informations_ de l'utilisateur du SI.
	- _Rechercher les information_s d'un utilisateur dans le SI selon son identifiant. 
	- _Obtenir la liste_ des utilisateurs du système.  
	- _Gérer la migration des scripts de base de données_ (création de schémas, insertion, mise à jour de tables ou de données ...).	
- un **Front-End** (Client Web) pour interagir avec le Back-End par le biais d'une interface utilisateur. Le front fournit les interfaces pour :
	- Se Connecter/Décconnecter à l'application.
	- Ajouter/Inscrire un nouvel utilisateur dans le SI.
	- Visualiser les informations des utilisateurs. 
	- Modifier les informations d'un utilisateur.
	- Supprimer les informations d'un utilisateur. 

**NB** :
- les données/informations sont stockées dans une base de données relationnelles
- Voir la section `Stack Technique` pour plus de détails sur l'ensemble des technos utilisées dans cette application.
- **SI** : Système d'informations.


## Spécifications 
Dans cette section, quelques éléments sont fournis pour faciliter la compréhension du besoin et des réalisations techniques.
La caractéristique principale de cette application est non seulement la gestion des **authentification et autorisation**, mais également de la **sécurisation des resources** de l'application.
Les élements des processus de gestion des autorisation, authentication et sécurisation des ressources (de l'application), seront mis en place à partir des spécifications
**JWT**, **OAuth2** avec **Spring Security**.
Par soucis de simplicité, le _socle architectural_ de l'application est du monolithe, les échanges se feront principalement entre le client (front-end) et le serveur (back-end). 
Les éléments ci-dessous sont fournis dans le cadre cette spécification :
- une brève présentation de **_OAuth2_** et **_JWT_** 
- le diagramme d'architecture applicative et technique
- les diagrammes de séquences de fonctionnement global des points suivants :
	- Accès aux resources protégées : demande été génration des jetons d'accès et d'actualisation
	- Ajout ou enregistrement de nouveaux utilisateurs dans l'application.
	- La gestion de la connexion des utilisateurs à l'application
- les schémas et modèles de données pour la gestion de la persistance des informations dans l'application.


### Brève Présentation de  OAuth2 et JWT  
Le tableau ci-dessous permet de résumer les rôles définis côté serveur par la spécification OAuth2 pour la gestion desd accès à des ressources sécurisées. 
|Rôle|Description|
|---|---|
|**Resource owner**|_Entité capable d'accorder ou de contrôler les accès à des ressources protégées (par exemple l'utilisateur final)_| 
|**Resource server**|_Serveur hébergeant des données/informations protégées, qui fournit réellement les ressources_|
|**Authorization server**|_Le serveur gérant le processus d'autorisation agissant comme intermédiaire entre le client et le propriétaire de la ressource_|

JWT (**J**SON **W**eb **T**oken), est une spécification pour la représentation des revendications (claims) à transférer entre deux parties. Les revendications sont codées en tant qu'objet JSON utilisé comme charge
 utile d'une structure chiffrée, permettant aux revendications d'être signées ou chiffrées numériquement. La structure peut être :
- **J**SON **W**eb **S**ignature (JWS) ou 
- **J**SON **W**eb **E**ncryption (JWE).

JWT peut être choisi comme format pour les jetons d'accès et d'actualisation utilisés dans le protocole OAuth2.


### Architecture Applicative et Technique Globale 
Le diagramme ci-dessous fournit une vision globale des flux d'échanges entre l'application et les acteurs du système et(ou) briques/composants applicatifs.
Elle comporte les éléments suivants :
- le **Back-End** qui embarque :
	- _Le serveur d'autorisation_ : intégrant le processus de validation des informations d'identification de l'utilisateur, production des jetons (jeton d'accès + jeton d'actualisation).
	- _Le Serveur de ressources_ : intégrant les points de terminaison de l'API REST à sécuriser et les éléments de gestion associés.
	- _Les différentes configurations_ de sécurisation des élements applicatifs.
- le **Front-End** : fournissant l'interface utilisateur permettant d'échanger.
- le **SGBD** : poour le stockage et la persistance des informations métiers de l'application.

![DAAT](./docs/images/architecture_technique_globale_monolithe.png "Diagrammme Architecture Applicatif et Technique")

### Le Fonctionnement Global de l'ajout d'un nouvel utilisateur
Le principe de fonctionnement de l'enrgistrement des informations d'un nouvel utilisateur dans le SI, est présenté par le diagramme de séquences ci-dessous :
![DS](./docs/images/fonctionnement_global_monolithe_signup_new_user.png "Diagramme de séquences Ajout nouvel utilisateur")

### Le Fonctionnement Global de la Connexion/Déconnexion
Le principe de fonctionnement de la connexion d'un utilisateur du SI avec ses informations, est présenté par le diagramme de séquences ci-dessous :
![DS](./docs/images/fonctionnement_global_monolithe_signin_user.png "Diagramme de séquences Connexion Utilisateur")

### Le Fonctionnement Global de des accès aux ressources de l'application
Une vue macroscopique du fonctionnement global de l'application est fournie par le diagramme de séquences ci-dessous. Il est composé de deux pricipales phases:
- La demande et obtention des jetons d'accès et d'actualisation
- L'accès proprement dit aux ressources de l'application avec le jeton d'accès.

![DS](./docs/images/fonctionnement_global_monolithe.png "Diagramme de séquences du fonctionnement global Accès aux ressources")

### Modèles et Schémas de données
Ce sont les informations qui permettront au système d'informations :
- de confirmer ou valider l'identité de tout acteur (humain ou système) qui tente d'accéder au système d'informations. 
- d'octroyer les droits d'accès au système d'informations (donc aux ressources) à tout acteur (humain ou système). 
Il comprend principalement :
- le modèle de données des informations d'identification des utilisateurs (sur la base des spécifications fournies par **Spring Security**)
- le modèle de données des informations d'identification des clients de l'application(sur la base des spécifications fournies par **JWT** et **OAuth2**)

#### Le modèle données des informations d'identification des utilisateurs 
Le diagramme de classes ci-dessous présente les relations entre les entités permettant de gérer l'identification des utilisateurs.
![DC](./docs/images/credentials_model_donnees_monolithe.png "Diagramme de Classes des objets de gestion de l'identification des utilisateurs")

#### Le modèle données des informations d'identification des clients
Le diagramme ci-dessous présente les tables OAuth2 pour la gestion des informations d'identifications des clients.
![MCD](./docs/images/model_donnees_tables_oauth2.png "MCD Tables OAuth2") 

## Stack Technique
Une liste non exhaustive des technos embarquées pour le développment de cette application :

![](https://img.shields.io/badge/Java_11-✓-blue.svg)
![](https://img.shields.io/badge/Maven_3-✓-blue.svg)
![](https://img.shields.io/badge/Spring_Boot_2-✓-blue.svg)
![](https://img.shields.io/badge/Spring_Security_5-✓-blue.svg)
![](https://img.shields.io/badge/OAuh2-✓-blue.svg)
![](https://img.shields.io/badge/JWT-✓-blue.svg)
![](https://img.shields.io/badge/Jpa_2-✓-blue.svg)
![](https://img.shields.io/badge/Hibernate_5-✓-blue.svg)
![](https://img.shields.io/badge/MariaDB-✓-blue.svg)
![](https://img.shields.io/badge/Flyway-✓-blue.svg)
![](https://img.shields.io/badge/Docker-✓-blue.svg)
![](https://img.shields.io/badge/Swagger_3-✓-blue.svg)
![](https://img.shields.io/badge/Model_Mapper-✓-blue.svg)
![](https://img.shields.io/badge/EhCache-✓-blue.svg)
![](https://img.shields.io/badge/Angular-✓-blue.svg)

- C'est un projet `Maven` avec `Spring Boot`
- Il utilise `Spring Security`, `JWT`, `OAuh2` pour la sécurisation des échanges (production de jetons, authentification et autorisation).
- Il utilise `H2`/`MariaDB`comme base de données aussi bien pour les Tests Unitaires que d'Intégration.
- Il utilise `Angular JS` pour l'interface utilisateur (le Clent Web).
- Il utilise `JPA / Hibernate` pour les concepts ORM et DAO.
- Il utilise `Flyway` pour la migration de bases de données.
- Il utilise `Docker` pour la containerisation.
- Il utilise `Swagger/OpenAPI 3` pour la documentation et tests de l'API.

## Réalisations Techniques 
Il s'agit des éléments techniques qui ont permis de développer ou implémenter les besoins exprimés. 

### Configurations
Cette section fournit l'ensemble des configurations pour l'exécution de l'application dans le cadre d'une exploitation. Afin de simplifier
l'exploitation, dans l'application les configurations suivantes sont fournies :
- Le fichier de configuration des propriétés de gestion des accès à la base de données
- Le fichier de configuration des autres propriétés applicatives

#### Configuration Base de données et migration de scripts avec Flyway
La gestion des propriétés d'accès à la base de données est founies par le fichier : [db.properties](/oauth2-jwt-rest-secure-server/src/main/resources/oauth2-jwt-rest-secure-db.properties).

```properties
# Configurations pour la migration de scripts DDL et DML avec Flyway
spring.flyway.enabled=true
spring.flyway.group=true
spring.flyway.baseline-on-migrate=true
spring.flyway.sql-migration-prefix=V
spring.flyway.sql-migration-separator=__
spring.flyway.sql-migration-suffixes=.sql
spring.flyway.locations=filesystem:docs/db/migration

# Configurations pour la gestion des accès à la base de données (seuls les élements principaux sont founis, les autres propriétés ressortent de l'affinage).
# Eléments de gestion de la datasource.
vot.datasource-props.type=com.zaxxer.hikari.HikariDataSource
vot.datasource-props.driver-class-name=org.mariadb.jdbc.Driver
vot.datasource-props.url=jdbc:mariadb://localhost:3306/jwtauth?useSSL=false
vot.datasource-props.jdbcUrl=${vot.datasource-props.url}
vot.datasource-props.datasource-class-name=org.mariadb.jdbc.MariaDbDataSource
vot.datasource-props.user-name=root
vot.datasource-props.password=<mot_de_passe>
vot.datasource-props.persistence-unit-name=OAuth2JwtRestSecureServerPU
vot.datasource-props.generate-ddl=false
vot.datasource-props.platform=MYSQL
# Gestionnaire pool de connexion à la base de données : Hikari
# Si vous désirez modifiez le nom du gestionnaire de pool Hikari.
vot.hikari-props.pool-name=OAuth2JwtRestSecureServerCP 

```

#### Configuration des autres propriétés applicatives
La gestion des autres proptiétés (hormis pour la basede données) est fournie par le fichier : [application.properties](/oauth2-jwt-rest-secure-server/src/main/resources/oauth2-jwt-rest-secure-application.properties).

```properties 
server.port=8500
server.servlet.context-path=/my-user-api

# Configurations pour OAuth2 et JWT : production, stockage et gestion des jetons, initialisation des tables OAuth2 de façon programmatique
# renseigne les clients à insérér en base séparés par une virgule (,). Le split se fait au niveau applicatif.
vot.oauth-props.client-id=my-user,my-user-admin,my-user-manager
vot.oauth-props.client-secret=VEeH+KOUaCldaLX9m5lnGTTzuwopqvJc/HjdeMQVT/pMCo+adI/e5ean7+b5actIKcVxqLyNSU6IYLDljtwDBQ==,xgCL1eyVvbIqRxRLuJmJWRQSsLsGkrCA7iG9swXyHNTiGBnSIhP+ixYN7Xw2e5ueuJ7A6mxI9JlKr0sYmVJMCw==,vNonAt5P0ULOZlyU3X1RGUKXDoZzN1q3+LLdZwVmwskZmVCRRW7jG4IV3zxyIhfK59p5qisA6RErA7WV4qhxbA==
# Le token d'accès est valide pendant 24 hours : 86400 seconds =24 hours * 60 minutes * 60 seconds, la conversion en millisecondes est effectuée au niveau applicatif.
vot.oauth-props.access-token-validity=86400 
# Le token d'actualisation est valide pendant 30 jours : 2592000 seconds = 30 * 24 hours * 60 minutes * 60 seconds
vot.oauth-props.refresh-token-validity=2592000
# Les types de subventions proposés 
vot.oauth-props.authorized-grant-types=password,refresh_token,client_credentials,authorization_code
# URL pour la génération des jetons (jeton d'accès et d'actualisation) et persistance en base de données selon la configuration proposée.
vot.oauth-props.access-token-uri=http://localhost:${server.port}/${server.servlet.context-path}/oauth/token

# Configurations pour la cryptographie : fourniture du magasin des clés et certificats (Keystore).
vot.crypto-props.keystore-file-location=./src/main/resources/crypto/my-user-keystore.jks
vot.crypto-props.public-key-file-location=./src/main/resources/crypto/my-user-publicKey.pem.txt
vot.crypto-props.keystore-password=my-user
vot.crypto-props.key-password=my-user
vot.crypto-props.alias-keystore=my-user

```

## Les Tests
Les outils de tests classiques de Java Spring sont utilisés pour effectuer des tests.

### Les Types de Tests
- _Tests unitaires_
- _Tests d'intégration_
- _Tests fonctionnels_

### Les Outils de Tests
Les outils de tests proposés ou utilisées sont les suivants :
- Outils de Tests de Spring Framework (spring-boot-starter-test) qui intègre:
	- spring-test, spring-boot-test, spring-boot-test-autoconfigure
	- JUnit 4+
	- Mockito
	- Assertions avec Assert-J,…
- Plugin JaCoCo maven (avec les plugin surefire et failsafe) pour produire le rapport de couverture de code.
- Postman pour tester l'API

### Rapport de couverture des tests
La couverture des tests est mesurée et fournie par JaCoCo. L'image ci-dessous fournit la couverture du code de l'application à l'exception des objets de couche de modèle.
TODO