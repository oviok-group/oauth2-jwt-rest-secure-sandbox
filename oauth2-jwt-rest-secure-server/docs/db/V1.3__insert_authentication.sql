/*  --------------------------------------
	-- DDL : Data Manipulation Language
	-- DATABASE : MARIADB 
	-- SCHEMA OR CATALOG :  
	-------------------------------------
*/
/* Inserion dans la table OAUTH_CLIENT_DETAILS */
INSERT INTO `OAUTH_CLIENT_DETAILS` (`CLIENT_ID`, `RESOURCE_IDS`, `CLIENT_SECRET`, `SCOPE`, `AUTHORIZED_GRANT_TYPES`, `WEB_SERVER_REDIRECT_URI`, `AUTHORITIES`, `ACCESS_TOKEN_VALIDITY`, `REFRESH_TOKEN_VALIDITY`, `ADDITIONAL_INFORMATION`, `AUTOAPPROVE`) 
VALUES
	('my-user', 'my-user-rest-api', '$2a$12$QgQXiQd0vgo4qdwU4YpxBOPomLXA50snGy46xdVeQOeFVFu2meeT2', 'read', 'password', '', 'ROLE_USER', 86400000, 86400000, '{"Utilisateur":null}', 'read'),
	('my-user-admin', 'my-user-rest-api', '$2a$12$Xyhj2awMmpAB1EY6lG3uxOHZYO6IhncZLK/m8hgtYmUIHa2hhmrCy', 'read,write', 'password', '', 'ROLE_ADMIN', 86400000, 86400000, '{"Administrateur":null}', 'read,write'),
	('my-user-manager', 'my-user-rest-api', '$2a$12$0bQ60v2oBNxr1EAr5pY4VOTSDRnPLvpy67RVPCEKtMT1vppJhqy1C', 'read,write', 'password', '', 'ROLE_USER_MANAGER', 86400000, 86400000, '{"Manager":null}', 'read,write'); 
	
	
/*  	
	('my-user', 'my-user-rest-api', '$2a$12$hMUSoeK53BnOGye5g0QBsOV.2sNm9nVVhjKm4t7zxZJtro2/a6OWW', 'read', 'password', 'http://localhost:8500/my-user-api/login', 'ROLE_USER', 86400000, 86400000, '{"Utilisateur":null}', 'read'),
	('my-user-admin', 'my-user-rest-api', '$2a$12$xNO/Jm44RzuY1pSNOQsGu.rQl3bdeE01rtEnoMErYDbNVr.N43szW', 'read,write', 'password', 'http://localhost:8500/my-user-api/login', 'ROLE_ADMIN', 86400000, 86400000, '{"Administrateur":null}', 'read,write'),
	('my-user-manager', 'my-user-rest-api', '$2a$12$oq7NUcH7twFkMo00VSFRFOZgJN2NvQZa5Eni5k0cpPVjoeHdjKELC', 'read,write', 'password', 'http://localhost:8500/my-user-api/login', 'ROLE_USER_MANAGER', 86400000, 86400000, '{"Manager":null}', 'read,write');
*/ 	
	


