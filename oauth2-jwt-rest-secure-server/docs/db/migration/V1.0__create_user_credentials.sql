/*  -------------------------------------
	-- DDL : Data Definition Language
	-- DATABASE : MARIADB 
	-- SCHEMA OR CATALOG :  
	-------------------------------------
*/
/* 	---------------------------
	-- Scripts de Suppressions 
	---------------------------
*/ 
 -- Suppression de la table T_USERS et éléments rattachés
DROP TABLE IF EXISTS hibernate_sequence;
DROP TABLE IF EXISTS USER_ROLES;
DROP TABLE IF EXISTS T_USERS;

/*  ----------------------------------------
	-- Scripts de Création des Credentials
	---------------------------------------
*/
-- Création de la séquence pour incrément automatique de l'identifiant de la table T_USERS
CREATE TABLE hibernate_sequence (
	next_val BIGINT(20)
);

-- Initialisation de la séquence 
INSERT INTO hibernate_sequence VALUES (1);

-- Création de la TABLE T_USERS
CREATE TABLE T_USERS (
	ID BIGINT(20) NOT NULL,
	ACCOUNT_EXPIRED BIT(1) NOT NULL,
	ACCOUNT_LOCKED BIT(1) NOT NULL,
	CREATED_TIME DATETIME,
	CREDENTIALS_EXPIRED BIT(1) NOT NULL,
	EMAIL VARCHAR(254) NOT NULL,
	ENABLED BIT(1) NOT NULL,
	USER_PASSWORD VARCHAR(60) NOT NULL,
	UPDATED_TIME DATETIME,
	USER_NAME VARCHAR(80) NOT NULL,
	OPTLOCK INTEGER NOT NULL DEFAULT '0',
	PRIMARY KEY (ID)
);

-- Création de la TABLE USERS_ROLES
CREATE TABLE USER_ROLES (
	USER_ID BIGINT(20) NOT NULL,
	ROLES INT(11)
);

-- Contrainte unicité de l'adresse électronique
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_1f8qpknpngd98342v0j2ceadc unique (EMAIL);

-- Contrainte unicité du login
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_5ebsdi5h725fqsiarxvgl1j43 unique (USER_NAME);

-- Contrainte de la clé étrangère dans la TABLE USERS_ROLES
ALTER TABLE USER_ROLES 
	ADD CONSTRAINT FKfpmqjl35c9jym3tpu09y3hyqq 
	FOREIGN KEY (USER_ID) 
	REFERENCES T_USERS (ID);