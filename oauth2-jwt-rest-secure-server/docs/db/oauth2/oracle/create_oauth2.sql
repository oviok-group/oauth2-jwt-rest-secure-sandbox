/*  -------------------------------------
	-- DDL : Data Definition Language
	-- DATABASE : MARIADB - MYSQL
	-- SCHEMA OR CATALOG :  
	-------------------------------------
*/
/* 	---------------------------
	-- Scripts de Suppressions 
	---------------------------
*/ 
-- Suppressions des tables OAUTH
DROP TABLE IF EXISTS OAUTH_CLIENT_DETAILS CASCADE;
DROP TABLE IF EXISTS OAUTH_CLIENT_TOKEN CASCADE;
DROP TABLE IF EXISTS OAUTH_ACCESS_TOKEN CASCADE;
DROP TABLE IF EXISTS OAUTH_REFRESH_TOKEN CASCADE;
DROP TABLE IF EXISTS OAUTH_CODE CASCADE;
DROP TABLE IF EXISTS OAUTH_APPROVALS CASCADE;

/*  ----------------------------------------
	-- Scripts de Création des tables OAUTH 
	---------------------------------------
*/
-- Création de la table OAUTH_CLIENT_DETAILS	
CREATE TABLE OAUTH_CLIENT_DETAILS (
  CLIENT_ID VARCHAR(255) PRIMARY KEY, 			-- OAuth 2.0 protocol client ID.
  RESOURCE_IDS VARCHAR(255),					-- Identifiers of the OAuth 2.0 resource servers.
  CLIENT_SECRET VARCHAR(255),					-- OAuth 2.0 protocol client secret.
  SCOPE VARCHAR(255),							-- OAuth 2.0 scopes, comma-separated values.
  AUTHORIZED_GRANT_TYPES VARCHAR(255),			-- OAuth 2.0 authorization grant types, comma-separated values.
  WEB_SERVER_REDIRECT_URI VARCHAR(255),			-- OAuth 2.0 redirect URIs, comma-separated values.
  AUTHORITIES VARCHAR(255),						-- OAuth 2.0 resource grant authorities.
  ACCESS_TOKEN_VALIDITY INTEGER,				-- Validity of the OAuth 2.0 access tokens, in seconds.
  REFRESH_TOKEN_VALIDITY INTEGER,				-- Validity of the OAuth 2.0 refresh tokens, in seconds.
  ADDITIONAL_INFORMATION VARCHAR(4096),			-- Field reserved for additional information about the client.
  AUTOAPPROVE VARCHAR(255)						-- Flag indicating if scopes should be automatically approved.
);

-- Création de la table OAUTH_CLIENT_TOKEN
CREATE TABLE OAUTH_CLIENT_TOKEN (
  TOKEN_ID VARCHAR(255),						-- Token ID.
  TOKEN BLOB,									-- Token value.
  AUTHENTICATION_ID VARCHAR(255) PRIMARY KEY,	-- Authentication ID related to client token.
  USER_NAME VARCHAR(255),						-- Username, identification of the user.
  CLIENT_ID VARCHAR(255)						-- OAuth 2.0 Client ID.
);

-- Création de la table OAUTH_ACCESS_TOKEN
CREATE TABLE OAUTH_ACCESS_TOKEN (
  TOKEN_ID VARCHAR(255),						-- Token ID.
  TOKEN BLOB,									-- Token value.
  AUTHENTICATION_ID VARCHAR(255) PRIMARY KEY,	-- Authentication ID related to access token.
  USER_NAME VARCHAR(255),						-- Username, identification of the user.
  CLIENT_ID VARCHAR(255),						-- OAuth 2.0 Client ID.
  AUTHENTICATION BLOB,							-- Encoded authentication details.
  REFRESH_TOKEN VARCHAR(255)					-- Refresh token ID.
);

-- Création de la table OAUTH_REFRESH_TOKEN
CREATE TABLE OAUTH_REFRESH_TOKEN (
  TOKEN_ID VARCHAR(255),						-- Refresh token ID.
  TOKEN BLOB,									-- Token value.
  AUTHENTICATION BLOB							-- Encoded authentication details.
);

-- Création de la table OAUTH_CODE
CREATE TABLE OAUTH_CODE (
  CODE VARCHAR(255),							-- OAuth 2.0 protocol "codes".
  AUTHENTICATION BLOB							-- Encoded authentication details.
);

-- Création de la table OAUTH_APPROVALS
CREATE TABLE OAUTH_APPROVALS (
  USERID VARCHAR(255),
  CLIENTID VARCHAR(255),
  SCOPE VARCHAR(255),
  STATUS VARCHAR(10),
  EXPIRESAT DATETIME,
  LASTMODIFIEDAT DATETIME
);