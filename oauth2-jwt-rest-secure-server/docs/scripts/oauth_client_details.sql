-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.2.11-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour jwtauth
CREATE DATABASE IF NOT EXISTS `jwtauth` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `jwtauth`;

-- Export de la structure de la table jwtauth. oauth_client_details
CREATE TABLE IF NOT EXISTS `oauth_client_details` (
  `CLIENT_ID` varchar(255) NOT NULL,
  `RESOURCE_IDS` varchar(255) DEFAULT NULL,
  `CLIENT_SECRET` varchar(255) DEFAULT NULL,
  `SCOPE` varchar(255) DEFAULT NULL,
  `AUTHORIZED_GRANT_TYPES` varchar(255) DEFAULT NULL,
  `WEB_SERVER_REDIRECT_URI` varchar(255) DEFAULT NULL,
  `AUTHORITIES` varchar(255) DEFAULT NULL,
  `ACCESS_TOKEN_VALIDITY` int(11) DEFAULT NULL,
  `REFRESH_TOKEN_VALIDITY` int(11) DEFAULT NULL,
  `ADDITIONAL_INFORMATION` varchar(4096) DEFAULT NULL,
  `AUTOAPPROVE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table jwtauth.oauth_client_details : ~3 rows (environ)
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` (`CLIENT_ID`, `RESOURCE_IDS`, `CLIENT_SECRET`, `SCOPE`, `AUTHORIZED_GRANT_TYPES`, `WEB_SERVER_REDIRECT_URI`, `AUTHORITIES`, `ACCESS_TOKEN_VALIDITY`, `REFRESH_TOKEN_VALIDITY`, `ADDITIONAL_INFORMATION`, `AUTOAPPROVE`) VALUES
	('my-user', 'my-user-rest-api', '$2a$12$pEqmW7BVK9PYbI8KA0XfCe/15n51tCH5BgNZ2R2SaK8KB52XQy8de', 'read', 'client_credentials', 'http://127.0.0.1', 'ROLE_USER', 86400000, 86400000, '{"Utilisateur":null}', 'read'),
	('my-user-admin', 'my-user-rest-api', '$2a$12$l.dgBH2YBu8kJvBfKXjwtODN/sjuwTTtSYrCMgUtmO11VM.K0.RKi', 'read,write', 'client_credentials,password,refresh_token,authorization_code', 'http://127.0.0.1', 'ROLE_ADMIN', 86400000, 86400000, '{"Administrateur":null}', 'read,write'),
	('my-user-manager', 'my-user-rest-api', '$2a$12$8SfS.ppev0X3rNOSXGTNve7zo4PjwlOghScWNZShFHn0s0Pm.2N.O', 'read,write', 'client_credentials,password,refresh_token,authorization_code', 'http://127.0.0.1', 'ROLE_USER_MANAGER', 86400000, 86400000, '{"Manager":null}', 'read,write');
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
