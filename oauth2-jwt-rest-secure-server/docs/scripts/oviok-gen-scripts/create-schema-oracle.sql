create sequence hibernate_sequence start with 1 increment by  1

    create table t_users (
       id number(19,0) not null,
        account_expired number(1,0) not null,
        account_locked number(1,0) not null,
        created_time timestamp,
        credentials_expired number(1,0) not null,
        email varchar2(254 char) not null,
        enabled number(1,0) not null,
        user_password varchar2(60 char) not null,
        updated_time timestamp,
        user_name varchar2(80 char) not null,
        optlock number(10,0) not null,
        primary key (id)
    )

    create table user_roles (
       user_id number(19,0) not null,
        roles number(10,0)
    )

    alter table t_users 
       add constraint UK_1f8qpknpngd98342v0j2ceadc unique (email)

    alter table t_users 
       add constraint UK_5ebsdi5h725fqsiarxvgl1j43 unique (user_name)

    alter table user_roles 
       add constraint FKfpmqjl35c9jym3tpu09y3hyqq 
       foreign key (user_id) 
       references t_users
