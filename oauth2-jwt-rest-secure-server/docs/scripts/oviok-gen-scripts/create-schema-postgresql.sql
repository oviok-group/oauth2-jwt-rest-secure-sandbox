create sequence hibernate_sequence start 1 increment 1

    create table t_users (
       id int8 not null,
        account_expired boolean not null,
        account_locked boolean not null,
        created_time timestamp,
        credentials_expired boolean not null,
        email varchar(254) not null,
        enabled boolean not null,
        user_password varchar(60) not null,
        updated_time timestamp,
        user_name varchar(80) not null,
        optlock int4 not null,
        primary key (id)
    )

    create table user_roles (
       user_id int8 not null,
        roles int4
    )

    alter table if exists t_users 
       add constraint UK_1f8qpknpngd98342v0j2ceadc unique (email)

    alter table if exists t_users 
       add constraint UK_5ebsdi5h725fqsiarxvgl1j43 unique (user_name)

    alter table if exists user_roles 
       add constraint FKfpmqjl35c9jym3tpu09y3hyqq 
       foreign key (user_id) 
       references t_users
