create sequence hibernate_sequence start with 1 increment by 1

    create table t_users (
       id bigint not null,
        account_expired bit not null,
        account_locked bit not null,
        created_time datetime2,
        credentials_expired bit not null,
        email varchar(254) not null,
        enabled bit not null,
        user_password varchar(60) not null,
        updated_time datetime2,
        user_name varchar(80) not null,
        optlock int not null,
        primary key (id)
    )

    create table user_roles (
       user_id bigint not null,
        roles int
    )

    alter table t_users 
       add constraint UK_1f8qpknpngd98342v0j2ceadc unique (email)

    alter table t_users 
       add constraint UK_5ebsdi5h725fqsiarxvgl1j43 unique (user_name)

    alter table user_roles 
       add constraint FKfpmqjl35c9jym3tpu09y3hyqq 
       foreign key (user_id) 
       references t_users
