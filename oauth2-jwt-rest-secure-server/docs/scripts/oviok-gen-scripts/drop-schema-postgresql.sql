
    alter table if exists user_roles 
       drop constraint if exists FKfpmqjl35c9jym3tpu09y3hyqq

    drop table if exists t_users cascade

    drop table if exists user_roles cascade

    drop sequence if exists hibernate_sequence
