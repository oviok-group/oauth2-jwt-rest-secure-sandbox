-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.2.11-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour jwtauth
CREATE DATABASE IF NOT EXISTS `jwtauth` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `jwtauth`;

-- Export de la structure de la table jwtauth. user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `USER_ID` bigint(20) NOT NULL,
  `ROLES` int(11) DEFAULT NULL,
  KEY `FKfpmqjl35c9jym3tpu09y3hyqq` (`USER_ID`),
  CONSTRAINT `FKfpmqjl35c9jym3tpu09y3hyqq` FOREIGN KEY (`USER_ID`) REFERENCES `t_users` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table jwtauth.user_roles : ~8 rows (environ)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`USER_ID`, `ROLES`) VALUES
	(1, 0),
	(2, 3),
	(3, 3),
	(4, 3),
	(5, 4),
	(6, 3),
	(7, 3),
	(8, 3);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
