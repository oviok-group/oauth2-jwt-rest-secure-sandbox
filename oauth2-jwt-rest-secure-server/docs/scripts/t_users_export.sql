-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.2.11-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour jwtauth
CREATE DATABASE IF NOT EXISTS `jwtauth` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `jwtauth`;

-- Export de la structure de la table jwtauth. t_users
CREATE TABLE IF NOT EXISTS `t_users` (
  `ID` bigint(20) NOT NULL,
  `ACCOUNT_EXPIRED` bit(1) NOT NULL,
  `ACCOUNT_LOCKED` bit(1) NOT NULL,
  `CREATED_TIME` datetime DEFAULT NULL,
  `CREDENTIALS_EXPIRED` bit(1) NOT NULL,
  `EMAIL` varchar(254) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  `USER_PASSWORD` varchar(60) NOT NULL,
  `UPDATED_TIME` datetime DEFAULT NULL,
  `USER_NAME` varchar(80) NOT NULL,
  `OPTLOCK` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_1f8qpknpngd98342v0j2ceadc` (`EMAIL`),
  UNIQUE KEY `UK_5ebsdi5h725fqsiarxvgl1j43` (`USER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table jwtauth.t_users : ~8 rows (environ)
/*!40000 ALTER TABLE `t_users` DISABLE KEYS */;
INSERT INTO `t_users` (`ID`, `ACCOUNT_EXPIRED`, `ACCOUNT_LOCKED`, `CREATED_TIME`, `CREDENTIALS_EXPIRED`, `EMAIL`, `ENABLED`, `USER_PASSWORD`, `UPDATED_TIME`, `USER_NAME`, `OPTLOCK`) VALUES
	(1, b'0', b'0', '2020-12-23 10:11:28', b'0', 'admin.test@live.fr', b'1', '$2a$12$3yOzX6o60.NQfdcGJAnamu0y2tNip6EzNxOvtM10ivD8cmwutrwzq', NULL, 'admin', 0),
	(2, b'0', b'0', '2020-12-23 10:11:28', b'0', 'client.test@live.fr', b'1', '$2a$12$uCa/OmS7PrNIgsWTe461yuVLYIjvuf8RWjl78.qJrcVxff5bJwsh6', NULL, 'client', 0),
	(3, b'0', b'0', '2020-12-23 10:11:28', b'0', 'client1.test@live.fr', b'1', '$2a$12$xrq6TOr2z7n9uiIwJY4cSu3kQAlW219lrm/Uh8EjZntZ3MzbrvufS', NULL, 'client1', 0),
	(4, b'0', b'0', '2020-12-23 10:11:28', b'0', 'client2.test@live.fr', b'1', '$2a$12$CnwL1k1xhSgACqybLN8sveVOlfKZBdA2NYWECjWDXEwZwVgP6rFdq', NULL, 'client2', 0),
	(5, b'0', b'0', '2020-12-23 10:11:28', b'0', 'manager.test@live.fr', b'1', '$2a$12$KYWMVz/dxbxeMQAaNNhhb.Xd1/okirBtEO5IlJCrt/CqnlGymHWsm', NULL, 'manager', 0),
	(6, b'1', b'1', '2020-12-23 10:11:29', b'1', 'client3.test@live.fr', b'0', '$2a$12$gOx360257du776Uy5NmzGOPqdpdRIO5BsdodQCpMpls3K4.iuE0Re', NULL, 'client3', 0),
	(7, b'1', b'1', '2020-12-23 10:11:29', b'1', 'client4.test@live.fr', b'0', '$2a$12$GlBl5S6Xho4gE4FPCC5EG.MJWdfxH8YjAFF7aR3Ob9LS6ZnjkVtJy', NULL, 'client4', 0),
	(8, b'1', b'1', '2020-12-23 10:11:29', b'1', 'client5.test@live.fr', b'0', '$2a$12$FjhZopQ/pzTkbcm3gQQqwuw3aAihSbmkiphZJ68tk4wsbal6MkwLS', NULL, 'client5', 0);
/*!40000 ALTER TABLE `t_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
