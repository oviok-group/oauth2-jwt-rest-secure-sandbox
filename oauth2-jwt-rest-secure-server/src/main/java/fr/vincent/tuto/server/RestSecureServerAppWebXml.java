/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : RestSecureServerAppWebXml.java
 * Date de création : 29 nov. 2020
 * Heure de création : 20:12:02
 * Package : fr.vincent.tuto.server
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Vincent Otchoun
 *
 */
public class RestSecureServerAppWebXml extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder)
    {
        return applicationBuilder.sources(RestSecureServerApplicationStarter.class);
    }

}
