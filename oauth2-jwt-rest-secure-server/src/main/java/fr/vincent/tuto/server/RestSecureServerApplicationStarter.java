/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : RestSecureServerApplicationStarter.java
 * Date de création : 29 nov. 2020
 * Heure de création : 20:07:17
 * Package : fr.vincent.tuto.server
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server;



import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.common.collect.Lists;

import fr.vincent.tuto.common.config.LogStartConfig;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.dao.UserDAO;
import fr.vincent.tuto.server.enumeration.Role;

/**
 * Le Starter du serveur de gestion des accès aux ressources sécurisées.
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
@Import(value = { AppRootConfig.class })
// @EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
// JdbcTemplateAutoConfiguration.class })
public class RestSecureServerApplicationStarter
{
    //
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder pwdEncoder;


    /**
     * @param args
     */
    public static void main(String[] args)
    {
        final SpringApplication application = new SpringApplication(RestSecureServerApplicationStarter.class);
        LogStartConfig.addDefaultProfile(application);

        final Environment environment = application.run(args).getEnvironment();

        LogStartConfig.logStartUp(environment);
    }

    /**
     * Initialisation de la base au démarrage de l'application. Il permet également d'initialiser les ressources base de
     * données pour les tests unitaires.
     * 
     * @return
     */
    // @Bean
    // CommandLineRunner init()
    // {
    // return args -> {
    // this.creerJeuDeDonnees().forEach(user -> this.userDAO.save(user));
    // };
    // }

    /**
     * Fournir le jeu de données pour intialisser la base de données pour les tests unitaires et d'intégration de façon
     * programmatique.
     * 
     * @return le jeu de données.
     */
    @SuppressWarnings("unused")
    private List<fr.vincent.tuto.server.model.po.User> creerJeuDeDonnees()
    {
        final List<fr.vincent.tuto.server.model.po.User> users = Lists.newArrayList();
        // ADMIN
        final fr.vincent.tuto.server.model.po.User admin = createActiveUser(Role.ROLE_ADMIN.getAuthority(), "admin",
        "admin_19511982#",
        "admin.test@live.fr");

        // CLIENT
        final fr.vincent.tuto.server.model.po.User client = createActiveUser(Role.ROLE_USER.getAuthority(), "client",
        "client_19511982#",
        "client.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client1 = createActiveUser(Role.ROLE_USER.getAuthority(), "client1",
        "client1_19511982#",
        "client1.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client2 = createActiveUser(Role.ROLE_USER.getAuthority(), "client2",
        "client2_19511982#",
        "client2.test@live.fr");

        // USER MANAGER
        final fr.vincent.tuto.server.model.po.User manager = createActiveUser(Role.ROLE_USER_MANAGER.getAuthority(),
        "manager",
        "manager_19511982#", "manager.test@live.fr");

        // CLIENT INACTIF
        final fr.vincent.tuto.server.model.po.User client3 = createInActiveUser(Role.ROLE_USER.getAuthority(),
        "client3",
        "client3_19511982#",
        "client3.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client4 = createInActiveUser(Role.ROLE_USER.getAuthority(),
        "client4",
        "client4_19511982#",
        "client4.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client5 = createInActiveUser(Role.ROLE_USER.getAuthority(),
        "client5",
        "client5_19511982#",
        "client5.test@live.fr");

        users.add(admin);
        users.add(client);
        users.add(client1);
        users.add(client2);
        users.add(manager);
        users.add(client3);
        users.add(client4);
        users.add(client5);

        return users;
    }

    /**
     * @param pRole
     * @param pUsername
     * @param pPwd
     * @param pEmail
     * @return
     */
    private fr.vincent.tuto.server.model.po.User createActiveUser(final String pRole, final String pUsername,
    final String pPwd, final String pEmail)
    {
        // ADMIN
        final Set<Role> adminRoles = new HashSet<>();
        adminRoles.add(Role.valueOf(pRole));
        final fr.vincent.tuto.server.model.po.User adminUser = new fr.vincent.tuto.server.model.po.User()//
        .username(pUsername)//
        .password(this.pwdEncoder.encode(pPwd))//
        .email(pEmail)//
        .accountExpired(Boolean.FALSE)//
        .accountLocked(Boolean.FALSE)//
        .credentialsExpired(Boolean.FALSE)//
        .enabled(Boolean.TRUE)//
        .roles(adminRoles)//
        .createdTime(LocalDateTime.now(ZoneId.systemDefault()))//
        .updatedTime(LocalDateTime.now(ZoneId.systemDefault()));
        adminUser.setVersion(Integer.valueOf(0));
        return adminUser;
    }

    /**
     * @param pRole
     * @param pUsername
     * @param pPwd
     * @param pEmail
     * @return
     */
    public fr.vincent.tuto.server.model.po.User createInActiveUser(final String pRole, final String pUsername,
    final String pPwd, final String pEmail)
    {
        // ACTIVE USER
        final Set<Role> activeUserRoles = new HashSet<>();
        activeUserRoles.add(Role.valueOf(pRole));
        final fr.vincent.tuto.server.model.po.User activeUser = new fr.vincent.tuto.server.model.po.User()//
        .username(pUsername)//
        .password(this.pwdEncoder.encode(pPwd))//
        .email(pEmail)//
        .accountExpired(Boolean.TRUE)//
        .accountLocked(Boolean.TRUE)//
        .credentialsExpired(Boolean.TRUE)//
        .enabled(Boolean.FALSE)//
        .roles(activeUserRoles)//
        .createdTime(LocalDateTime.now(ZoneId.systemDefault()))//
        .updatedTime(LocalDateTime.now(ZoneId.systemDefault()));
        activeUser.setVersion(Integer.valueOf(0));
        return activeUser;
    }
}
