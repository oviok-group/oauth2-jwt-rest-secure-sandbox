/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 2 janv. 2021
 * Heure de création : 10:16:39
 * Package : fr.vincent.tuto.server.api
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient lescomposants d'exposition des fonctionnalités de l'application.
 * 
 * @author Vincent
 */
package fr.vincent.tuto.server.api;
