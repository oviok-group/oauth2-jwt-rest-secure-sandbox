/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AppRootConfig.java
 * Date de création : 30 nov. 2020
 * Heure de création : 07:11:41
 * Package : fr.vincent.tuto.server.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import fr.vincent.tuto.common.config.CommonBeansConfig;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.DatabasePropsService;

/**
 * Configuration de base. Elle contient les beans de niveua supérieur et tout configuration requise par des fiultres.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@Import(value = { JavaMailSenderImpl.class, ApplicationPropsService.class, CommonBeansConfig.class, DatabasePropsService.class })
@PropertySources(value = { @PropertySource(value = { "classpath:oauth2-jwt-rest-secure-db.properties" }, ignoreResourceNotFound = false), @PropertySource(value = {
        "classpath:oauth2-jwt-rest-secure-application.properties" }, ignoreResourceNotFound = false) })
@ComponentScan(basePackages = { "fr.vincent.tuto.server", "fr.vincent.tuto.common" })
@ConfigurationProperties(prefix = "vot", ignoreUnknownFields = true, ignoreInvalidFields = false)
@EntityScan("fr.vincent.tuto.server.model")
@EnableJpaRepositories(basePackages = "fr.vincent.tuto.server.dao", entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager")
// @ComponentScan(basePackages = "fr.vincent.tuto", excludeFilters = {
// @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CommonAppBaseConfig.class) })
public class AppRootConfig
{
    //
}
