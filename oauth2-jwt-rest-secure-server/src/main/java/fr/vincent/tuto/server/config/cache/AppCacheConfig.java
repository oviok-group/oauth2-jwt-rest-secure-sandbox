/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AppCacheConfig.java
 * Date de création : 30 déc. 2020
 * Heure de création : 15:23:46
 * Package : fr.vincent.tuto.server.config.cache
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.cache;

import java.time.Duration;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.EhcacheProps;
import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.model.po.User;

/**
 * Configuration pour optimiser les accès aux données avec `EhCache` dans l'API. Elle est inspirée de ce que propose
 * JHipster.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableCaching
public class AppCacheConfig
{
    //
    @Autowired
    private ApplicationPropsService propsService;

    // Créez cette configuration en tant que bean afin qu'elle soit utilisée pour personnaliser les caches créés
    // automatiquement
    // https://stackoverflow.com/questions/59679175/configuring-caching-for-hibernate-with-spring-boot-2-1
    @Bean
    public javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration()
    {
        final EhcacheProps ehcacheProps = this.propsService.getEhcacheProps();
        return Eh107Configuration.fromEhcacheCacheConfiguration(CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
        ResourcePoolsBuilder.heap(ehcacheProps.getMaxEntries()))//
        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcacheProps.getTimeToLiveSeconds())))//
        .build());
    }

    /**
     * @param cacheManager
     * @return
     */
    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager)
    {
        return hibernateProperties -> hibernateProperties.put("hibernate.javax.cache.cache_manager", cacheManager);
    }

    /**
     * @return
     */
    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer()
    {
        return cm -> {
            createCache(cm, ServerConstants.USERS_BY_USERNAME_CACHE);
            createCache(cm, ServerConstants.USERS_BY_EMAIL_CACHE);
            createCache(cm, User.class.getName());
            createCache(cm, User.class.getName() + ".roles");
        };
    }

    /**
     * Créer un nouveau cache.
     * 
     * @param cm        le gestionnaire de cache.
     * @param cacheName le nom du cache.
     */
    private void createCache(javax.cache.CacheManager cm, String cacheName)
    {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null)
        {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, this.jcacheConfiguration());
    }
}
