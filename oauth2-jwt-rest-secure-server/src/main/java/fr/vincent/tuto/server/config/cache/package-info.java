/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 déc. 2020
 * Heure de création : 15:22:59
 * Package : fr.vincent.tuto.server.config.cache
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Configurations pour optimiser les accès aux données.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.config.cache;
