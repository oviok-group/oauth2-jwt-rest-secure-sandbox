/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 14:24:40
 * Package : fr.vincent.tuto.server.config.db
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Configurations de base pour les accès aux informations en base de données.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.config.db;
