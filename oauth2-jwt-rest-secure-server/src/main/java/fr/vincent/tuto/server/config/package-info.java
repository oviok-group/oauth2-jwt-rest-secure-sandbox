/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 14:24:06
 * Package : fr.vincent.tuto.server.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Les configurations de base de l'application.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.config;
