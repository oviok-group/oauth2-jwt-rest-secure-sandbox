/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AuthServerOAuth2Config.java
 * Date de création : 24 déc. 2020
 * Heure de création : 10:00:08
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.security;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.util.Assert;

import com.zaxxer.hikari.HikariDataSource;

import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.service.security.CustomUserDetailsService;

/**
 * Configuration du point d'entrée de l'authorisation par OAuth2 dans l'application.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableAuthorizationServer
@Import(ServerSecurityConfig.class)
public class AuthServerOAuth2Config extends AuthorizationServerConfigurerAdapter implements InitializingBean
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServerOAuth2Config.class);

    //
    private String clientId;
    private String clientSecret;
    private int accessTokenMilliSeconds;
    private int refreshTokenMilliSeconds;
    private String[] authorizedGrantTypes;

    @Autowired
    private ApplicationPropsService propsService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("userDetailsService")
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private ApprovalStore approvalStore;

    @Autowired
    private DefaultTokenServices tokenServices;

    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;

    @Autowired
    @Qualifier("dataSource")
    private HikariDataSource dataSource;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception
    {
        // https://stackoverflow.com/questions/29510759/how-to-test-spring-security-oauth2-resource-server-security

        // https://stackoverflow.com/questions/35039656/how-to-add-a-client-using-jdbc-for-clientdetailsserviceconfigurer-in-spring

        // The grant_type should be one of authorization_code, client_credentials or password (for implicit use
        // response_type=token)

        this.afterPropertiesSet();
        final String[] clientIds = StringUtils.split(this.clientId, ServerConstants.COMMA_SPLIT_SEPARATOR);
        final String[] clientSecrets = StringUtils.split(this.clientSecret, ServerConstants.COMMA_SPLIT_SEPARATOR);

        Assert.notNull(clientIds, "Le tableau des identifiants ne derait pas être null.");
        Assert.notNull(clientSecrets, "Le tableau des `secret` ne derait pas être null.");

        clients//
        .jdbc(this.dataSource)// ;//

        /*
         * Insertion de base de données de façon programmatique. Au démarrage de l'application, il faut accéder à : oauth/token
         * pour créer le jeton d'accès. Alternative, utiliser un script DML pour insérer les données en base.
         */
        .withClient(clientIds[0].trim())//
        .secret(this.passwordEncoder.encode(clientSecrets[0].trim())) //
        .authorizedGrantTypes(this.authorizedGrantTypes)//
        .accessTokenValiditySeconds(this.accessTokenMilliSeconds)//
        .refreshTokenValiditySeconds(this.refreshTokenMilliSeconds)//
        .scopes(ServerConstants.SCOPE_SECURITY_CONFIG)//
        .authorities(Role.ROLE_CLIENT.getAuthority())//
        .autoApprove(true)//
        .additionalInformation(ServerConstants.USER_ADITIONAL, ServerConstants.USER_ADITIONAL)//
        .resourceIds(ServerConstants.RESOURCES_ID)//

        .and()//
        .withClient(clientIds[1].trim())//
        .secret(this.passwordEncoder.encode(clientSecrets[1].trim())) //
        .authorizedGrantTypes(this.authorizedGrantTypes)//
        .accessTokenValiditySeconds(this.accessTokenMilliSeconds)//
        .refreshTokenValiditySeconds(this.refreshTokenMilliSeconds)//
        .scopes(ServerConstants.SCOPE_SECURITY_CONFIG)//
        .authorities(Role.ROLE_CLIENT.getAuthority())//
        .autoApprove(true)//
        .additionalInformation(ServerConstants.ADMIN_ADITIONAL, ServerConstants.ADMIN_ADITIONAL)//
        .resourceIds(ServerConstants.RESOURCES_ID)//

        .and()//
        .withClient(clientIds[2].trim())//
        .secret(this.passwordEncoder.encode(clientSecrets[2].trim())) //
        .authorizedGrantTypes(this.authorizedGrantTypes)//
        .accessTokenValiditySeconds(this.accessTokenMilliSeconds)//
        .refreshTokenValiditySeconds(this.refreshTokenMilliSeconds)//
        .scopes(ServerConstants.SCOPE_SECURITY_CONFIG)//
        .authorities(Role.ROLE_CLIENT.getAuthority())//
        .autoApprove(true)//
        .additionalInformation(ServerConstants.MANAGER_ADITIONAL, ServerConstants.MANAGER_ADITIONAL)//
        .resourceIds(ServerConstants.RESOURCES_ID)//

        // .and()//
        // .build()//
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception
    {
        security//
        .tokenKeyAccess(ServerConstants.TOKEN_ACCESS_KEY)//
        .checkTokenAccess(ServerConstants.CHECK_TOKEN_ACCESS)//
        .passwordEncoder(this.passwordEncoder)//
        .allowFormAuthenticationForClients()//
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception
    {
        // Defines the authorization and token endpoints and the token services.
        endpoints//
        .authenticationManager(this.authenticationManager)//
        .userDetailsService(this.userDetailsService)//
        .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)//
        .accessTokenConverter(this.accessTokenConverter)//
        .approvalStore(this.approvalStore)//
        .tokenStore(this.tokenStore)//
        .tokenServices(this.tokenServices)//
        ;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        LOGGER.info("[afterPropertiesSet] - Initialiser les propriétés.");

        // initialisation des champs ou paramètres de service.
        this.clientId = this.propsService.getOauthProps().getClientId().trim();
        this.clientSecret = this.propsService.getOauthProps().getClientSecret().trim();

        // Convertir les validités des jetons en millsecondes
        this.accessTokenMilliSeconds = 1000 * this.propsService.getOauthProps().getAccessTokenValidity();
        this.refreshTokenMilliSeconds = 1000 * this.propsService.getOauthProps().getRefreshTokenValidity();
        final String athorizedGrantTypesStr = this.propsService.getOauthProps().getAuthorizedGrantTypes().trim();
        this.authorizedGrantTypes = StringUtils.split(athorizedGrantTypesStr, ServerConstants.COMMA_SPLIT_SEPARATOR);
    }
}
