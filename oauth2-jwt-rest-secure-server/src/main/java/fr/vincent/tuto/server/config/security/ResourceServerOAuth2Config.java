/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : ResourceServerOAuth2Config.java
 * Date de création : 26 déc. 2020
 * Heure de création : 06:30:38
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import fr.vincent.tuto.common.exception.AuthEntryPointCommon;
import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.enumeration.Role;

/**
 * Configuration pour la protection des accès aux ressources.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableResourceServer
public class ResourceServerOAuth2Config extends ResourceServerConfigurerAdapter
{

    @Autowired
    private AuthEntryPointCommon authEntryPointCommon;

    @Autowired
    private OAuth2AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private ResourceServerTokenServices tokenServices;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources//
        .authenticationEntryPoint(this.authEntryPointCommon)//
        .accessDeniedHandler(this.accessDeniedHandler)//
        .tokenServices(this.tokenServices)//
        .tokenStore(this.tokenStore)//
        .resourceId(ServerConstants.RESOURCES_ID);//
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        // La méthode remplacée définit quels chemins d'URL doivent être sécurisés et lesquels ne doivent pas l'être

        http//
        .requestMatchers()//
        .and()//
        .authorizeRequests()//

        .antMatchers(ServerConstants.SIGN_IN_PATTERN, ServerConstants.SIGN_UP_PATTERN, ServerConstants.UPDATE_PASWWORD).permitAll()//
        .antMatchers(ServerConstants.USERS_PATTERN).hasAnyAuthority(Role.ROLE_ADMIN.getAuthority())//
        .antMatchers(ServerConstants.USERS_MANAGE_PATTERN).hasAnyAuthority(Role.ROLE_ADMIN.getAuthority(), Role.ROLE_USER_MANAGER.getAuthority())//

        .antMatchers(ServerConstants.ROOT_PATTERN).authenticated()//
        .anyRequest().authenticated()//

        .and()//
        .headers().addHeaderWriter((request, response) -> {
            response.addHeader(ServerConstants.ALOW_ORIGIN, ServerConstants.ORIGIN);
            if (request.getMethod().equals(ServerConstants.OPTIONS_METHODS))
            {
                response.setHeader(ServerConstants.ALLOW_METHODS, request.getHeader(ServerConstants.REQUEST_METHOD));
                response.setHeader(ServerConstants.ALLOW_HEADERS, request.getHeader(ServerConstants.REQUEST_HEADER));
            }
        })//

        // Gestion des exceptions
        .and()//
        .exceptionHandling().authenticationEntryPoint(this.authEntryPointCommon).accessDeniedHandler(this.accessDeniedHandler)//
        ;
    }
}
