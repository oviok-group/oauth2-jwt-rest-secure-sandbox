/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : ServerSecurityConfig.java
 * Date de création : 3 déc. 2020
 * Heure de création : 09:10:47
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.security;

import java.io.File;
import java.io.InputStream;
import java.security.KeyPair;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.util.ResourceUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.google.common.io.Files;
import com.zaxxer.hikari.HikariDataSource;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.model.mo.CryptoModel;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil;
import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.service.security.CustomUserDetailsService;

/**
 * Configuration de la sécurité Web pour le serveur d'application.
 * <p>
 * <ul>
 * <li>Activer la configuration de la sécurité Web globale de l'application avec 'EnableWebSecurity'.</li>
 * <li>Activer la configuration de la sécurité sur les méthodes avec 'EnableGlobalMethodSecurity'. Le paramètre
 * proxyTargetClass est ajouté pour que cela fonctionne au niveau des controller (les contrôleurs sont généralement des
 * classes, n'implémentant aucune interface.).</li>
 * <li>Fournir les éléments suivants :
 * <ul>
 * <li>L'encodeur de mot de passe pour encoder nos mots de passe {@link BCryptPasswordEncoder}.</li>
 * <li>Le gestionnaire d'authentification.</li>
 * <li>La configuration de la sécurité pour les chemins (URL) publiés avec {@link WebSecurity}.</li>
 * </ul>
 * </li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableWebSecurity(debug = true)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, proxyTargetClass = true)
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter implements InitializingBean
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerSecurityConfig.class);

    private KeyPair keyPair;
    private String publicKeyAsString;

    private CryptoModel cryptoModel;
    private String keystoreFileLocation;
    private String aliasKeystore;
    private String storePassword;
    private String keyPassword;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("dataSource")
    private HikariDataSource dataSource;

    @Autowired
    @Qualifier("userDetailsService")
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private ApplicationPropsService propsService;

    // DECLARATIONS DES BEANS
    @Bean
    public TokenStore tokenStore()
    {
        return new JdbcTokenStore(this.dataSource);
    }

    @Bean
    public ApprovalStore approvalStore()
    {
        // return new JdbcApprovalStore(this.dataSource);
        TokenApprovalStore tokenApprovalStore = new TokenApprovalStore();
        tokenApprovalStore.setTokenStore(this.tokenStore());
        return tokenApprovalStore;
    }

    @Bean
    @Qualifier("accessDeniedHandler")
    public OAuth2AccessDeniedHandler accessDeniedHandler()
    {
        return new OAuth2AccessDeniedHandler();
    }

    @Bean
    @Primary
    public JwtAccessTokenConverter accessTokenConverter() throws Exception
    {
        LOGGER.info("[accessTokenConverter] - Configurer JwtAccessTokenConverter");

        final JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
        accessTokenConverter.setKeyPair(this.keyPair);
        accessTokenConverter.setVerifierKey(this.publicKeyAsString);// La clé publique
        accessTokenConverter.afterPropertiesSet();
        return accessTokenConverter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() throws Exception
    {
        LOGGER.info("[tokenServices] - Configurer DefaultTokenServices");

        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(this.tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setTokenEnhancer(this.accessTokenConverter());

        defaultTokenServices.setReuseRefreshToken(false);// S'il faut réutiliser les jetons d'actualisation (jusqu'à leur expiration).
        defaultTokenServices.setAccessTokenValiditySeconds(1000 * this.propsService.getOauthProps().getAccessTokenValidity());
        defaultTokenServices.setRefreshTokenValiditySeconds(1000 * this.propsService.getOauthProps().getRefreshTokenValidity());
        defaultTokenServices.setClientDetailsService(this.clientDetailsService);

        //
        defaultTokenServices.setAuthenticationManager(authentication -> {
            final UserDetails userDetails = this.userDetailsService.loadUserByUsername(authentication.getName());
            PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken(authentication.getName(), authentication.getCredentials(), userDetails
            .getAuthorities());
            token.setDetails(userDetails);
            return token;
        });

        // defaultTokenServices.setAuthenticationManager(this.authenticationManagerBean());

        defaultTokenServices.afterPropertiesSet();
        return defaultTokenServices;
    }

    /**
     * Implémentation d'AuthenticationProvider qui récupère les détails de l'utilisateur à partir d'un
     * {@link UserDetailsService}.
     * 
     * @return le fournisseur d'authentification.
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider()
    {
        final DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(this.passwordEncoder);
        provider.setUserDetailsService(this.userDetailsService);
        return provider;
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration(ServerConstants.BASE_URL, config);
        source.registerCorsConfiguration(ServerConstants.ROOT_PATTERN, config);
        source.registerCorsConfiguration(ServerConstants.V3_DOCS_SWAGGER, config);
        // source.registerCorsConfiguration("/my-user-api/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception
    {
        return super.authenticationManager();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        // Méthode remplacée pour définir la manière dont les utilisateurs sont authentifiés.
        auth//
        .jdbcAuthentication()//
        .dataSource(this.dataSource)// Indique que nous utilisons l'authentification JDBC
        .passwordEncoder(this.passwordEncoder)// Indique que nous utilisons un encodeur de mot de passe pour encoder nos mots
        // de passe. (Un bean est créé pour renvoyer le choix du mot de passe Encoder, nous utilisons BCrypt dans ce cas)
        .usersByUsernameQuery(ServerConstants.AUTH_USERS_QUERY)//
        .and()//
        .authenticationProvider(this.authenticationProvider())//
        ;

        auth.authenticationProvider(this.authenticationProvider());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        // La méthode remplacée définit quels chemins d'URL doivent être sécurisés et lesquels ne doivent pas l'être
        http//
        .cors().disable()//
        .csrf().disable()// Désactiver CSRF (cross site request forgery : falsification de demande intersite)
        .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).disable()//
        .anonymous().disable()//
        .headers().frameOptions().disable()//

        //
        .and()//
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Aucune session ne sera créée ou utilisée par Spring Security

        .and()//
        .authorizeRequests()//

        // Autoriser l'accès à swagger sans authentification
        // .antMatchers(ServerConstants.SWAGGER_URL_PATHS).permitAll()//
        // .antMatchers(ServerConstants.V3_DOCS_SWAGGER, ServerConstants.RES_SWAGGER, ServerConstants.CONFIG_SWAGGER,
        // ServerConstants.WEB_JARS)
        // .permitAll()//

        //
        // .antMatchers(ServerConstants.TOKEN_URL, ServerConstants.REFRESH_URL, ServerConstants.AUTHORIZE_URL,
        // ServerConstants.REDIRECT_URL,
        // ServerConstants.CHECK_URL, ServerConstants.KEY_URL, ServerConstants.LOGIN_URL,
        // ServerConstants.ERROR_URL).permitAll()//

        .antMatchers(ServerConstants.INDEX_SWAGGER).hasAuthority(Role.ROLE_ADMIN.getAuthority())//

        // .anyRequest().authenticated()//

        // .and()//
        // .formLogin().permitAll()// Configure une connexion basée sur un formulaire
        //
        // .and()//
        // .httpBasic()//
        //
        // .and()//
        // .userDetailsService(this.userDetailsService)//
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(WebSecurity web) throws Exception
    {
        // super.configure(web);

        // Autoriser l'accès à swagger sans authentification
        web.ignoring()//
        // .antMatchers(ServerConstants.TOKEN_URL)//
        .antMatchers(HttpMethod.OPTIONS, ServerConstants.BASE_URL)//
        .antMatchers(ServerConstants.V3_DOCS_SWAGGER)//
        .antMatchers(ServerConstants.INDEX_SWAGGER)//
        .antMatchers(ServerConstants.WEB_JARS)//
        .antMatchers(ServerConstants.RES_SWAGGER)//
        // .antMatchers(ServerConstants.CONFIG_SWAGGER)//

        //
        // // Base de données H2 non sécurisée (à des fins de test, la console H2 ne doit pas être non protégée en
        // // production)
        .and().ignoring().antMatchers(ServerConstants.H2_CONSOLE_URL)//
        .and().ignoring().antMatchers(ServerConstants.ACTUATOR_URL)//
        ;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        // Chargement des propriétés de gestion des éléments de cryptographie.
        this.keystoreFileLocation = this.propsService.getCryptoProps().getKeystoreFileLocation().trim();

        this.aliasKeystore = this.propsService.getCryptoProps().getAliasKeystore().trim();
        this.storePassword = this.propsService.getCryptoProps().getKeystorePassword().trim();
        this.keyPassword = this.propsService.getCryptoProps().getKeyPassword().trim();

        // Charger le magasin pour exploitation de ses éléments.
        this.cryptoModel = new CryptoModel()//
        .aliasKeystore(this.aliasKeystore)//
        .keystorePassword(this.storePassword)//
        .keyPassword(this.keyPassword)//
        ;

        this.keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keystoreFileLocation, this.cryptoModel);

        final String fileLocation = this.propsService.getCryptoProps().getPublicKeyFileLocation().trim();
        final File publicKeyFile = ResourceUtils.getFile(fileLocation);
        final InputStream inputStream = Files.asByteSource(publicKeyFile).openBufferedStream();
        this.publicKeyAsString = IOUtils.toString(inputStream, AppConstants.UTF_8);
    }
}
