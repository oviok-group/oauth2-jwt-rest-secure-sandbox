/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 3 déc. 2020
 * Heure de création : 09:02:19
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les objets de configurations de la sécurité pour le serveur.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.config.security;
