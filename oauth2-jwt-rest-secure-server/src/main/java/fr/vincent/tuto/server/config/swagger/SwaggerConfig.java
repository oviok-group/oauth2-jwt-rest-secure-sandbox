/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : SwaggerConfig.java
 * Date de création : 5 janv. 2021
 * Heure de création : 17:35:09
 * Package : fr.vincent.tuto.server.config.swagger
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.swagger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import com.google.common.collect.Lists;

import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.server.constant.ServerConstants;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;

/**
 * Configuration Swaager pour la production de la documentation et tests de l'API.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig
{
    //
    @Value("${vot.oauth-props.access-token-uri}")
    private String accessTokenUri;

    @Autowired
    private ApplicationPropsService propsService;

    @Bean
    public Docket myUserApi()
    {
        //
        return new Docket(DocumentationType.OAS_30)//
        // .groupName("default")//
        .select()//
        .apis(RequestHandlerSelectors.any())//
        .paths(PathSelectors.any())//
        .build()//
        .securityContexts(Collections.singletonList(this.securityContext()))//
        // .securitySchemes(Arrays.asList(this.apiKey(), this.apiCookieKey(), this.apiBearer()))//
        .securitySchemes(Arrays.asList(this.securitySchema(), this.apiKey(), this.apiCookieKey(), this.apiBearer()))//
        .apiInfo(this.metadata())//
        .produces(ServerConstants.mediaTypeSet)//
        .consumes(ServerConstants.mediaTypeSet)//
        ;
    }

    // @Bean
    // public Docket myUserApi()
    // {
    // return new Docket(DocumentationType.OAS_30)//
    // .select()//
    // .apis(RequestHandlerSelectors.any())//
    // // .apis(RequestHandlerSelectors.basePackage(AuthAppConstant.ACCOUNT_CONTROLLER_BASE_PACKAGE))//
    // .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))// allows selection of RequestHandler's
    // .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//
    // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
    // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiResponses.class))//
    // .paths(PathSelectors.any())//
    // .build()//
    // .apiInfo(this.metadata())//
    // .produces(ServerConstants.mediaTypeSet)//
    // .consumes(ServerConstants.mediaTypeSet)//
    // .securityContexts(Collections.singletonList(this.securityContext()))//
    // .securitySchemes(Arrays.asList(this.securitySchema(), this.apiKey(), this.apiCookieKey(), this.apiBearer()))//
    // // .apiInfo(apiInfo())
    // .directModelSubstitute(java.time.LocalDate.class, String.class)// substitutes LocalDate with String
    // .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)// substitutes ZonedDateTime with Date
    // .directModelSubstitute(java.time.LocalDateTime.class, Date.class)// substitutes LocalDateTime with Date
    // .genericModelSubstitutes(ResponseEntity.class)// essayer avec Optional
    // .genericModelSubstitutes(Optional.class)//
    // ;
    // }

    @Bean
    public SecurityScheme apiKey()
    {
        return new ApiKey(HttpHeaders.AUTHORIZATION, "apiKey", ServerConstants.HEADER);
    }

    @Bean
    public SecurityScheme apiCookieKey()
    {
        return new ApiKey(HttpHeaders.COOKIE, "apiKey", "cookie");
    }

    @Bean
    public ApiKey apiBearer()
    {
        // return new ApiKey(AuthAppConstant.AUTH_BAERER_TOKEN, AuthAppConstant.AUTHORISATION, AuthAppConstant.HEADER);
        return new ApiKey(ServerConstants.AUTH_BAERER_TOKEN, HttpHeaders.AUTHORIZATION, ServerConstants.HEADER);
    }

    @SuppressWarnings("deprecation")
    @Bean
    public SecurityConfiguration security()
    {
        // return new SecurityConfiguration("client", "secret", "", "", "Bearer access token", ApiKeyVehicle.HEADER,
        // HttpHeaders.AUTHORIZATION, "");
        return new SecurityConfiguration("client", "secret", "", "", "Bearer access token", ApiKeyVehicle.HEADER, HttpHeaders.AUTHORIZATION, "");
    }

    /**
     * Fournir les méta-données de la documentation de l'API.
     * 
     * @return les méta-données de la documentation.
     */
    private ApiInfo metadata()
    {
        return new ApiInfoBuilder()//
        .title(this.propsService.getSwaggerProps().getTitle().trim())//
        .description(this.propsService.getSwaggerProps().getDescription().trim())//
        .termsOfServiceUrl(this.propsService.getSwaggerProps().getTermeOfServiceUrl())//
        .contact(this.contact())//
        .license(this.propsService.getSwaggerProps().getLicence().trim())//
        .licenseUrl(this.propsService.getSwaggerProps().getLicenceUrl().trim())//
        .version(this.propsService.getSwaggerProps().getVersion().trim())//
        .build();
    }

    /**
     * Construire les informations de contact pour la documentation de l'API.
     * 
     * @return les informations de contact.
     */
    private Contact contact()
    {
        return new Contact(this.propsService.getSwaggerProps().getContactName().trim(), //
        this.propsService.getSwaggerProps().getContactUrl().trim(), //
        this.propsService.getSwaggerProps().getContactEmail().trim());
    }

    /**
     * @return
     */
    private OAuth securitySchema()
    {
        List<AuthorizationScope> authorizationScopeList = Lists.newArrayList();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));
        authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));

        List<GrantType> grantTypes = Lists.newArrayList();
        GrantType passwordCredentialsGrant = new ResourceOwnerPasswordCredentialsGrant(accessTokenUri);
        grantTypes.add(passwordCredentialsGrant);

        return new OAuth("oauth2", authorizationScopeList, grantTypes);
    }

    /**
     * @return
     */
    private SecurityContext securityContext()
    {
        return SecurityContext.builder()//
        .securityReferences(defaultAuth())//
        .build();
    }

    /**
     * @return
     */
    private List<SecurityReference> defaultAuth()
    {
        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[3];
        authorizationScopes[0] = new AuthorizationScope("read", "read all");
        authorizationScopes[1] = new AuthorizationScope("trust", "trust all");
        authorizationScopes[2] = new AuthorizationScope("write", "write all");
        // return Collections.singletonList(new SecurityReference("oauth2", authorizationScopes));

        return Arrays.asList(new SecurityReference(HttpHeaders.AUTHORIZATION, authorizationScopes), new SecurityReference(
        ServerConstants.AUTH_BAERER_TOKEN, authorizationScopes));

        // AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        // AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        // authorizationScopes[0] = authorizationScope;
        // return Arrays.asList(new SecurityReference(HttpHeaders.AUTHORIZATION, authorizationScopes));
    }
}
