/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 5 janv. 2021
 * Heure de création : 17:32:35
 * Package : fr.vincent.tuto.server.config.swagger
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Configuration des beans pour la production de la documentation et test de l'API avec Swagger.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.config.swagger;
