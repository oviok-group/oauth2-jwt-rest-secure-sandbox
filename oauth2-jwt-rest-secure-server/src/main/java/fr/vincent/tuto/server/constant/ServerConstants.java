/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : ServerConstants.java
 * Date de création : 1 déc. 2020
 * Heure de création : 08:16:49
 * Package : fr.vincent.tuto.server.constant
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.constant;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.google.common.collect.Sets;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.server.enumeration.Role;

/**
 * Utilitaire de défintion des constantes pour le module applicatif.
 * 
 * @author Vincent Otchoun
 */
public final class ServerConstants
{
    /**
     * Constructeur privé de l'utilitaire
     */
    private ServerConstants()
    {
    }

    // private static final String RESOURCE_ID = "resource-server-rest-api";

    public static final String RESOURCES_ID = "my-user-api";
    public static final String ROOT_PATTERN = "/api/**";
    // public static final String SIGNIN_ALL_PATTERN = "/api/signin**";
    public static final String SIGN_IN_PATTERN = "/api/signin/**"; // pour se connecter
    public static final String SIGN_UP_PATTERN = "/api/register/**"; // pour créer un nouvel utilisateur (signup)
    public static final String UPDATE_PASWWORD = "/api/updatePassword/**";
    public static final String USERS_PATTERN = "/api/users**";
    public static final String USERS_MANAGE_PATTERN = "/api/users/manage**";

    public static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    public static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";

    public static final String REDIRECT_URI = "http://localhost:8500/my-user-api/login";
    public static final String USER_ADITIONAL = "Utilisateur";
    public static final String ADMIN_ADITIONAL = "Administrateur";
    public static final String MANAGER_ADITIONAL = "Manager";

    public static final String KEYSTORE_LOCATION = "my-user-keystore.jks";
    public static final String COMMA_SPLIT_SEPARATOR = ",";
    public static final String SCOPE_SECURITY_CONFIG = "read,write,trust";
    public static final String READ_SCOPE_SECURITY_CONFIG = "read";

    public static final String TOKEN_ACCESS_KEY = "permitAll()";
    public static final String CHECK_TOKEN_ACCESS = "isAuthenticated()";

    public static final String ROOT_CONTEXT = "/";
    public static final String BASE_URL = "/**";
    public static final String TOKEN_URL = "/oauth/token";
    public static final String REFRESH_URL = "/oauth/refresh";
    public static final String AUTHORIZE_URL = "oauth/authorize";
    public static final String REDIRECT_URL = "oauth/redirect";
    public static final String CHECK_URL = "oauth/check_token";
    public static final String KEY_URL = "oauth/token_key";
    public static final String LOGIN_URL = "/login**";
    public static final String ERROR_URL = "/error**";

    public static final String AUTH_USERS_QUERY = "select user_name, user_password, email, enabled from t_users where user_name = ?";

    // Cross-Origin - CORS constants
    public static final String ALOW_ORIGIN = HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;
    public static final String ORIGIN = "*";
    public static final String ALLOW_CREDENTIALS = HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS;
    // public static final String CREDENTIALS = "true";
    public static final String ALLOW_METHODS = HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS;

    // DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT
    // public static final String METHODS = "{GET, POST, PUT, DELETE, OPTION, PATCH, HEAD}";
    public static final String ALLOW_AGE = HttpHeaders.ACCESS_CONTROL_MAX_AGE;
    // public static final String MAX_AGE = "3600";
    public static final String ALLOW_HEADERS = HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS;

    // public static final String HEADERS = "Origin, X-Requested-With, Remember-me, Authorization, Content-Type,
    // Authorization, credential, X-XSRF-TOKEN";
    public static final String REQUEST_METHOD = HttpHeaders.ACCESS_CONTROL_REQUEST_METHOD;
    public static final String REQUEST_HEADER = HttpHeaders.ACCESS_CONTROL_REQUEST_HEADERS;
    public static final String OPTIONS_METHODS = "OPTIONS";

    // DAO
    public static final String USERS_BY_USERNAME_CACHE = "usersByUsername";
    public static final String USERS_BY_EMAIL_CACHE = "usersByEmail";
    public static final String ATTRIBUTE_PATHS = "roles";

    // REGEX ET PATTERNS
    public static final Pattern UUID_PATTERN = AppConstants.UUID_PATTERN;
    public static final String PASSWORD_REGEX = AppConstants.PASSWORD_PATTERN;
    public static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);
    public static final String EMAIL_REGEX = AppConstants.EMAIL_PATTERN;
    public static final Pattern EMAI_PATTERN = Pattern.compile(EMAIL_REGEX);
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";
    public static final Pattern LOGIN_PATTERN = Pattern.compile(LOGIN_REGEX);

    // USER Model Validation et messages
    public static final String USERNAME_VALIDATION_MSG = "Veuillez fournir un nom d'utilisateur entre 3 et 80 caractères";
    public static final String PWD_VALIDATION_MSG = "Veuillez fournir un mot de passe de 8 caractères ou plus avec au moins 1 chiffre et 1 lettre";
    public static final String EMAIL_VALIDATION_MSG = "Veuillez fournir un email valide";

    public static final String ACCOUNT_MSG_DEF = "Indique si le compte de l'utilisateur a expiré.";
    public static final String ID_MSG_DEF = "Identifiant technique auto-généré de l'objet en base.";
    public static final String LOCKED_MSG_DEF = "Indique si l'utilisateur est verrouillé ou déverrouillé.";
    public static final String CREDENTIALS_MSG_DEF = "Indique si les informations d'identification de l'utilisateur (mot de passe)ont expiré.";
    public static final String ENABLED_MSG_DEF = "Indique si l'utilisateur est activé ou désactivé.";
    public static final String OPTLOCK_MSG_DEF = "Lock Optimiste, 0 par défaut.";

    // SWAGGER
    public static final String[] SWAGGER_URL_PATHS = new String[] { "/swagger-ui/index.html", "/swagger-resources/**", "/v3/api-docs**",
            "/webjars/**" };
    public static final String V2_DOCS_SWAGGER = "/v2/api-docs";
    public static final String V3_DOCS_SWAGGER = "/v3/api-docs";
    public static final String INDEX_SWAGGER = "/swagger-ui/index.html";
    public static final String RES_SWAGGER = "/swagger-resources/**";
    public static final String UI_SWAGGER = "/swagger-ui.html";
    public static final String CONFIG_SWAGGER = "/configuration/**";
    public static final String WEB_JARS = "/webjars/**";
    public static final String PUBLIC_SWAGGER = "/configuration/**";

    public static final String H2_CONSOLE_URL = "/h2-console/**/**";
    public static final String ACTUATOR_URL = "/actuator/**/**";

    public static final String HEADER = "Header";
    public static final String AUTH_BAERER_TOKEN = "Bearer %token";

    public static Set<String> mediaTypeSet = Sets.newHashSet();
    static
    {
        mediaTypeSet.add(MediaType.APPLICATION_JSON_VALUE);
    }

    public static final String USER_MSG = "L'utilisateur : ";
    public static final String USER_MSG_NOT_ACTIVATED = " est désactivé il ne peut être authentifié.";
    public static final String MAIL_MSG = "L'utilisateur avec l'email : ";
    public static final String DB_MSG = " n'est pas trouvé en base de données.";

    // Fonctions utilitaires
    /**
     * Creation de Spring Security User.
     * 
     * @param pUsername login de l'utilisateur.
     * @param pUser     les informations de l'utilisateur en base de données.
     * @return le user de Spring Security.
     */
    public static User createSpringSecurityUser(final String pUsername, final fr.vincent.tuto.server.model.po.User pUser)
    {
        //
        final Boolean active = pUser.getEnabled();
        if (BooleanUtils.isFalse(active))
        {
            throw new CustomAppException(USER_MSG + pUsername + USER_MSG_NOT_ACTIVATED);
        }

        Set<SimpleGrantedAuthority> grantedAuthorities = pUser.getRoles()//
        .stream()//
        .map(role -> new SimpleGrantedAuthority(role.getAuthority()))//
        .collect(Collectors.toSet());
        return new User(pUser.getUsername(), pUser.getPassword(), grantedAuthorities);
    }

    /**
     * Convertit les autorités en une liste d'objets GrantedAuthority.
     */
    public static final java.util.List<GrantedAuthority> ROLES = AuthorityUtils.createAuthorityList(Role.ROLE_ANONYMOUS.getAuthority(),
    Role.ROLE_ADMIN.getAuthority(), Role.ROLE_USER.getAuthority(), Role.ROLE_CLIENT.getAuthority(), Role.ROLE_USER_MANAGER.getAuthority());
}
