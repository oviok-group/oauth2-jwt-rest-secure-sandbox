/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 1 déc. 2020
 * Heure de création : 08:15:19
 * Package : fr.vincent.tuto.server.constant
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Uitlitaires de défition des constantes.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.constant;
