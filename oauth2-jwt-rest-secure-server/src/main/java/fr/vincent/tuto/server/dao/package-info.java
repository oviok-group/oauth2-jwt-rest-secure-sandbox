/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 21:58:47
 * Package : fr.vincent.tuto.server.dao
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Composants des objets de la couche d'abstraction d'accès à la base dee données.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.dao;
