/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : Role.java
 * Date de création : 30 nov. 2020
 * Heure de création : 21:09:01
 * Package : fr.vincent.tuto.server.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.enumeration;

import org.springframework.security.core.GrantedAuthority;

/**
 * Enumeration définissant les différents rôles (Authority) dans l'application.
 * 
 * @author Vincent Otchoun
 */
public enum Role implements GrantedAuthority
{
    ROLE_ADMIN, // dispose des autorisations complètes sur l'ensemble des données, indice=0.
    ROLE_CLIENT, // acteur authentifié se connectant à l'application, indice=1.
    ROLE_ANONYMOUS, // Utilisateur non authentifié, indice=2.
    ROLE_USER, // Utilisateur authentifié et disposant des autorisations complètes sur les données possédées, indice=3.
    ROLE_USER_MANAGER // Gestionnaire d'utilisateurs : dispose d'autorisations sur les données utilisateur, indice=4.
    ;

    @Override
    public String getAuthority()
    {
        return name();
    }
}
