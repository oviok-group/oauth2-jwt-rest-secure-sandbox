/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 21:04:31
 * Package : fr.vincent.tuto.server.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les types de données particulier restraignant les valeurs possibles de variables.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.enumeration;
