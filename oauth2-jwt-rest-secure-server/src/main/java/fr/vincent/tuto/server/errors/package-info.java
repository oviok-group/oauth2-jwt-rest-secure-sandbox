/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 11 déc. 2020
 * Heure de création : 04:43:09
 * Package : fr.vincent.tuto.server.errors
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les objets de gestion des exceptions.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.errors;
