/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 14:29:41
 * Package : fr.vincent.tuto.server.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Le modèle de données des objets persistants ou non.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.model;
