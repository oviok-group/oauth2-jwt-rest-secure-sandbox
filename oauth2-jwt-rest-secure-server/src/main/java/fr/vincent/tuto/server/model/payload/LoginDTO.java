/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : LoginDTO.java
 * Date de création : 30 déc. 2020
 * Heure de création : 14:10:05
 * Package : fr.vincent.tuto.server.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.model.payload;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.vincent.tuto.server.constant.ServerConstants;
import io.swagger.annotations.ApiModelProperty;

/**
 * Mapping des informations de connexion de l'utilisateur.
 * 
 * @author Vincent Otchoun
 */
public class LoginDTO implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -772932019150041867L;

    @NotBlank(message = ServerConstants.USERNAME_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.LOGIN_REGEX)
    @Size(min = 3, max = 80, message = ServerConstants.USERNAME_VALIDATION_MSG)
    @ApiModelProperty(name = "username", dataType = "java.lang.String", value = "Login authentification de l'utilisateur", position = 0)
    private String username;

    @NotNull(message = ServerConstants.PWD_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.PASSWORD_REGEX, message = ServerConstants.PWD_VALIDATION_MSG)
    @Size(min = 60, max = 60)
    @ApiModelProperty(name = "password", dataType = "java.lang.String", value = "Mot de passe authentification de l'utilisateur", position = 1)
    private String password;

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
