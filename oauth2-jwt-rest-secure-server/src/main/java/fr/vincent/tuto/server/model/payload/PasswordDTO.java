/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : PasswordDTO.java
 * Date de création : 30 déc. 2020
 * Heure de création : 11:35:09
 * Package : fr.vincent.tuto.server.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.model.payload;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.vincent.tuto.server.constant.ServerConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Mapping des informations de changement du mot de passe de l'utilisateur.
 * 
 * @author Vincent Otchoun
 */
@ApiModel(description = "DTO pour le changement du mot de passe de l'utilisateur", value = "Données mot de passe")
public class PasswordDTO implements Serializable, Comparable<PasswordDTO>
{
    /**
     * 
     */
    private static final long serialVersionUID = -2651878946339804754L;

    @NotNull(message = ServerConstants.PWD_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.PASSWORD_REGEX, message = ServerConstants.PWD_VALIDATION_MSG)
    @Size(min = 60, max = 60)
    @ApiModelProperty(name = "currentPassword", dataType = "java.lang.String", value = "Mot de passe actuel de l'utilisateur", position = 0)
    private String currentPassword;

    @NotNull(message = ServerConstants.PWD_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.PASSWORD_REGEX, message = ServerConstants.PWD_VALIDATION_MSG)
    @Size(min = 60, max = 60)
    @ApiModelProperty(name = "newPassword", dataType = "java.lang.String", value = "Nouveau mot de passe actuel de l'utilisateur", position = 1)
    private String newPassword;

    public String getCurrentPassword()
    {
        return this.currentPassword;
    }

    public String getNewPassword()
    {
        return this.newPassword;
    }

    public void setCurrentPassword(String currentPassword)
    {
        this.currentPassword = currentPassword;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int compareTo(PasswordDTO o)
    {
        return this.currentPassword.compareToIgnoreCase(o.currentPassword);
    }
}
