/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : UserDTO.java
 * Date de création : 4 déc. 2020
 * Heure de création : 14:57:34
 * Package : fr.vincent.tuto.server.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server.model.payload;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.model.po.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Le DTO de mapping des informations de l'objet persistant {@link User}.
 * 
 * @author Vincent Otchoun
 */
@ApiModel(description = "DTO informations utilisateur", value = "Données Utilisateur")
public class UserDTO implements Serializable, Comparable<UserDTO>
{
    /**
     * 
     */
    private static final long serialVersionUID = 897952097395010927L;

    @ApiModelProperty(name = "id", dataType = "java.lang.Long", value = "Identifiant technique auto-généré", position = 0)
    private Long id;

    @NotBlank(message = ServerConstants.USERNAME_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.LOGIN_REGEX)
    @Size(min = 3, max = 80, message = ServerConstants.USERNAME_VALIDATION_MSG)
    @ApiModelProperty(name = "username", dataType = "java.lang.String", value = "Login authentification de l'utilisateur", position = 1)
    private String username;

    @NotNull(message = ServerConstants.PWD_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.PASSWORD_REGEX, message = ServerConstants.PWD_VALIDATION_MSG)
    @Size(min = 60, max = 60)
    @ApiModelProperty(name = "password", dataType = "java.lang.String", value = "Mot de passe authentification de l'utilisateur", position = 2)
    private String password;

    @Email(message = ServerConstants.EMAIL_VALIDATION_MSG)
    @Size(min = 8, max = 254, message = ServerConstants.EMAIL_VALIDATION_MSG)
    @ApiModelProperty(name = "email", dataType = "java.lang.String", value = "Adresse mail de l'utilisateur", position = 3)
    private String email;

    @ApiModelProperty(name = "accountExpired", dataType = "java.lang.Boolean", value = "Indique si le compte de l'utilisateur a expiré ou non.", position = 4)
    private Boolean accountExpired;

    @ApiModelProperty(name = "accountLocked", dataType = "java.lang.Boolean", value = "Indique si l'utilisateur est verrouillé ou non.", position = 5)
    private Boolean accountLocked;

    @ApiModelProperty(name = "credentialsExpired", dataType = "java.lang.Boolean", value = "Indique si les informations d'identification de l'utilisateur ont expiré ou non", position = 6)
    private Boolean credentialsExpired; // .

    @ApiModelProperty(name = "enabled", dataType = "java.lang.Boolean", value = "Indique si l'utilisateur est activé ou désactivé ou non", position = 7)
    private Boolean enabled; //

    @ApiModelProperty(name = "roles", dataType = "java.util.Set", value = "Les rôles de l'utilisateur.", position = 8, example = "ADMIN,CLIENT,ANONYMOUS,USER,USER_MANAGER")
    private Set<String> roles;

    private LocalDateTime createdTime; // horodatage pour la création de l'objet en base de données.

    private LocalDateTime updatedTime; // horodatage pour la modification de l'objet en base en base de données.

    //////////////////////
    ///// ACCESSEURS
    //////////////////////

    public Long getId()
    {
        return this.id;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getEmail()
    {
        return this.email;
    }

    public Boolean getAccountExpired()
    {
        return this.accountExpired;
    }

    public Boolean getAccountLocked()
    {
        return this.accountLocked;
    }

    public Boolean getCredentialsExpired()
    {
        return this.credentialsExpired;
    }

    public Boolean getEnabled()
    {
        return this.enabled;
    }

    public Set<String> getRoles()
    {
        return this.roles;
    }

    public LocalDateTime getCreatedTime()
    {
        return this.createdTime;
    }

    public LocalDateTime getUpdatedTime()
    {
        return this.updatedTime;
    }

    public void setId(final Long pId)
    {
        this.id = pId;
    }

    public void setUsername(final String pUsername)
    {
        this.username = pUsername;
    }

    public void setPassword(final String pPassword)
    {
        this.password = pPassword;
    }

    public void setEmail(final String pEmail)
    {
        this.email = pEmail;
    }

    public void setAccountExpired(final Boolean pAccountExpired)
    {
        this.accountExpired = pAccountExpired;
    }

    public void setAccountLocked(final Boolean pAccountLocked)
    {
        this.accountLocked = pAccountLocked;
    }

    public void setCredentialsExpired(final Boolean pCredentialsExpired)
    {
        this.credentialsExpired = pCredentialsExpired;
    }

    public void setEnabled(final Boolean pEnabled)
    {
        this.enabled = pEnabled;
    }

    public void setRoles(final Set<String> pRoles)
    {
        this.roles = pRoles;
    }

    public void setCreatedTime(final LocalDateTime pCreatedTime)
    {
        this.createdTime = pCreatedTime;
    }

    public void setUpdatedTime(final LocalDateTime pUpdatedTime)
    {
        this.updatedTime = pUpdatedTime;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int compareTo(UserDTO o)
    {
        return this.username.compareToIgnoreCase(o.username);
    }
}
