/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 22:12:55
 * Package : fr.vincent.tuto.server.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Objts non persistants.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.model.payload;
