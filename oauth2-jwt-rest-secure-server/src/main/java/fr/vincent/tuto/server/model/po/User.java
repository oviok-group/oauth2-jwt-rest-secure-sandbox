/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : User.java
 * Date de création : 30 nov. 2020
 * Heure de création : 22:19:04
 * Package : fr.vincent.tuto.server.model.po
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.model.po;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.enumeration.Role;

/**
 * Mapping objet des informations en base de données de la table T_USERS. Les informations de mapping sont pour la
 * plupart au sens Spring Security {@link UserDetails}. Hormis : email,createdTime, updatedTime).
 * 
 * @author Vincent Otchoun
 */
@Entity
@Table(name = "T_USERS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends AbstractPersistable<Long> implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -2091030230524126354L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id; // identifiant technique auto-généré de l'objet en base.

    @NotNull(message = ServerConstants.USERNAME_VALIDATION_MSG)
    @Size(min = 3, max = 80, message = ServerConstants.USERNAME_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.LOGIN_REGEX)
    @Column(name = "USER_NAME", nullable = false, unique = true)
    private String username; // le login utilisé pour authentifier l'utilisateur (non null et unique).

    // @JsonIgnore
    @NotNull(message = ServerConstants.PWD_VALIDATION_MSG)
    @Pattern(regexp = ServerConstants.PASSWORD_REGEX, message = ServerConstants.PWD_VALIDATION_MSG)
    @Size(min = 60, max = 60)
    @Column(name = "USER_PASSWORD", length = 60, nullable = false)
    private String password; // le mot de passe utilisé pour authentifier l'utilisateur(non null).

    @Email(message = ServerConstants.EMAIL_VALIDATION_MSG)
    @Size(min = 8, max = 254, message = ServerConstants.EMAIL_VALIDATION_MSG)
    @Column(name = "EMAIL", unique = true, length = 254, nullable = false)
    private String email; // adresse mail de l'utilisateur.

    @Column(name = "ACCOUNT_EXPIRED", nullable = false)
    private Boolean accountExpired; // Indique si le compte de l'utilisateur a expiré. Un compte expiré ne peut pas être
                                    // authentifié.

    @Column(name = "ACCOUNT_LOCKED", nullable = false)
    private Boolean accountLocked;// Indique si l'utilisateur est verrouillé ou déverrouillé. Un utilisateur verrouillé
                                  // ne peut pas être authentifié.

    @Column(name = "CREDENTIALS_EXPIRED", nullable = false)
    private Boolean credentialsExpired; // Indique si les informations d'identification de l'utilisateur (mot de
                                        // passe)ont expiré. Les informations d'identification expirées empêchent
                                        // l'authentification.

    @Column(name = "ENABLED", nullable = false)
    private Boolean enabled; // Indique si l'utilisateur est activé ou désactivé. Un utilisateur désactivé ne peut pas
                             // être authentifié.

    // @JsonIgnore
    @ElementCollection(fetch = FetchType.EAGER)
    @OrderBy
    private Set<Role> roles = new HashSet<>();

    @Column(name = " CREATED_TIME", insertable = true, updatable = false)
    private LocalDateTime createdTime; // horodatage pour la création de l'objet en base.

    @Column(name = "UPDATED_TIME", insertable = false, updatable = true)
    private LocalDateTime updatedTime; // horodatage pour la modification de l'objet en base.

    @JsonIgnore
    @Version
    @Column(name = "OPTLOCK", nullable = false)
    private Integer version; // Gestion de l'optimistic lock (lock optimiste).

    @PrePersist
    protected void onCreate()
    {
        this.accountExpired = Boolean.FALSE;
        this.accountLocked = Boolean.FALSE;
        this.credentialsExpired = Boolean.FALSE;
        this.enabled = Boolean.TRUE;
        this.createdTime = LocalDateTime.now(ZoneId.systemDefault());
        this.updatedTime = LocalDateTime.now(ZoneId.systemDefault());
        this.version = Integer.valueOf(0);
    }

    @PreUpdate
    protected void onUpdate()
    {
        this.updatedTime = LocalDateTime.now(ZoneId.systemDefault());
    }

    // BUILDER
    public User id(final Long pId)
    {
        this.id = pId;
        return this;
    }

    public User username(final String pUsername)
    {
        this.username = pUsername;
        return this;
    }

    public User password(final String pPassword)
    {
        this.password = pPassword;
        return this;
    }

    public User email(final String pEmail)
    {
        this.email = pEmail;
        return this;
    }

    public User accountExpired(final Boolean pAccountExpired)
    {
        this.accountExpired = pAccountExpired;
        return this;
    }

    public User accountLocked(final Boolean pAccountLocked)
    {
        this.accountLocked = pAccountLocked;
        return this;
    }

    public User credentialsExpired(final Boolean pCredentialsExpired)
    {
        this.credentialsExpired = pCredentialsExpired;
        return this;
    }

    public User enabled(final Boolean pEnabled)
    {
        this.enabled = pEnabled;
        return this;
    }

    public User roles(final Set<Role> pRoles)
    {
        this.roles = pRoles;
        return this;
    }

    public User createdTime(final LocalDateTime pDateCreation)
    {
        this.createdTime = pDateCreation;
        return this;
    }

    public User updatedTime(final LocalDateTime pUpdateDate)
    {
        this.updatedTime = pUpdateDate;
        return this;
    }

    ///////////////////
    //// ACCESSEURS
    ///////////////////

    @Override
    public Long getId()
    {
        return this.id;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getEmail()
    {
        return this.email;
    }

    public Boolean getAccountExpired()
    {
        return this.accountExpired;
    }

    public Boolean getAccountLocked()
    {
        return this.accountLocked;
    }

    public Boolean getCredentialsExpired()
    {
        return this.credentialsExpired;
    }

    public Boolean getEnabled()
    {
        return this.enabled;
    }

    public Set<Role> getRoles()
    {
        return this.roles;
    }

    public LocalDateTime getCreatedTime()
    {
        return this.createdTime;
    }

    public LocalDateTime getUpdatedTime()
    {
        return this.updatedTime;
    }

    public Integer getVersion()
    {
        return this.version;
    }

    @Override
    public void setId(final Long pPk)
    {
        this.id = pPk;
    }

    public void setUsername(final String pUsername)
    {
        this.username = pUsername;
    }

    public void setPassword(final String pPassword)
    {
        this.password = pPassword;
    }

    public void setEmail(final String pEmail)
    {
        this.email = pEmail;
    }

    public void setAccountExpired(final Boolean pAccountExpired)
    {
        this.accountExpired = pAccountExpired;
    }

    public void setAccountLocked(final Boolean pAccountLocked)
    {
        this.accountLocked = pAccountLocked;
    }

    public void setCredentialsExpired(final Boolean pCredentialsExpired)
    {
        this.credentialsExpired = pCredentialsExpired;
    }

    public void setEnabled(final Boolean pEnabled)
    {
        this.enabled = pEnabled;
    }

    public void setRoles(final Set<Role> pRoles)
    {
        this.roles = pRoles;
    }

    public void setCreatedTime(final LocalDateTime pCreatedTime)
    {
        this.createdTime = pCreatedTime;
    }

    public void setUpdatedTime(final LocalDateTime pUpdatedTime)
    {
        this.updatedTime = pUpdatedTime;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(Object pObj)
    {
        if (this == pObj)
        {
            return true;
        }
        if (pObj == null || getClass() != pObj.getClass())
        {
            return false;
        }
        //
        User other = (User) pObj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
