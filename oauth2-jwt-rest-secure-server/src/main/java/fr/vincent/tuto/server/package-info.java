/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 30 nov. 2020
 * Heure de création : 14:23:21
 * Package : fr.vincent.tuto.server
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Package de base des sources du projet.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server;
