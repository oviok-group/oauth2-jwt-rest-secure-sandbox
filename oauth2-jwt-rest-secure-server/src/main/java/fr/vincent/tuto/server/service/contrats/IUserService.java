/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : IUserService.java
 * Date de création : 4 janv. 2021
 * Heure de création : 22:26:03
 * Package : fr.vincent.tuto.server.service.contrats
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.service.contrats;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.vincent.tuto.server.model.po.User;

/**
 * @author Vincent Otchoun
 */
public interface IUserService
{

    /**
     * Ajouter les informations d'un utilisateur dans le système d'informations.
     * 
     * @param pUser les informations de l'utilisateur à persister.
     * @return les informations de l'utilisateur ajoutée.
     */
    User createNewUser(final User pUser);

    /**
     * Metter à jour les informations d'un utilisateur dans le système après une recherche par son identifiant.
     * 
     * @param pId   identifiant technique de l'utilisateur dans le système (le critère de recherche).
     * @param pUser les informations de l'utilisateur à mettre à jour.
     * @return les informations de l'utilisateur mis à jour.
     */
    User updateUser(final Long pId, final User pUser);

    /**
     * Modifier le mot de passe de l'utilisateur.
     * 
     * @param pCurrentPassword actuel mot de passe de l'utilisateur.
     * @param pNewPassword     le nouveau mot de passe de l'utilisateur.
     */
    void changePassword(final String pCurrentPassword, final String pNewPassword);

    /**
     * Obtenir les informations d'un utilisateur par son identifiant.
     * 
     * @param pUsername le login de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserByUsername(final String pUsername);

    /**
     * Obtenir les informations d'un utilisateur par son adresse électronique en ignorant la casse.
     * 
     * @param pEmail adresse électronique de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserByEmailIgnoreCase(final String pEmail);

    /**
     * Rechercher l'existence des informations d'un utilisateur par son identifiant.
     * 
     * @param pUsername le login de l'utilisateur recherché (le critère de recherche).
     * @return true si existe, false sinon.
     */
    Boolean existsUserByUsername(final String pUsername);

    /**
     * Rechercher l'existence des informations d'un utilisateur par son adresse électronique.
     * 
     * @param pEmail adresse électronique de l'utilisateur recherché (le critère de recherche).
     * @return true si existe, false sinon.
     */
    Boolean existsUserByEmail(final String pEmail);

    /**
     * Obtenir les informations d'un utilisateur par son identifiant technique.
     * 
     * @param id identifiant technique de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserById(final Long id);

    /**
     * Obtenir les informations d'un utilisateur par son identifiant technique avec projection sur son rôle.
     * 
     * @param pId identifiant technique de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserWithRolesById(final Long pId);

    /**
     * Obtenir les informations d'un utilisateur par son identifiant en ignorant la casse avec projection sur son rôle.
     * 
     * @param pUsername le login de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserWithRolesByUsernameIgnoreCase(final String pUsername);

    /**
     * Obtenir les informations d'un utilisateur par son adresse électronique en ignorant la casse avec projection sur son
     * rôle.
     * 
     * @param pEmail adresse électronique de l'utilisateur recherché (le critère de recherche).
     * @return les informations de l'utlisateur correspondant si trouvé, vide sinon.
     */
    Optional<User> findUserWithRolesByEmailIgnoreCase(final String pEmail);

    /**
     * Obtenir la liste paginée des informations des utilisateurs du système d'informations.
     * 
     * @param pPageable condition de pagination de la liste (index de la page, nombre d'éléments dans la page à retourner).
     * @return la liste paginée des informations recherchées.
     */
    Page<User> getAllManagedUsers(final Pageable pPageable);

    /**
     * Obtenir la liste paginée des informations des utilisateurs à partir du login.
     * 
     * @param pUsername le login de l'utilisateur (le critère de recherche).
     * @param pPageable condition de pagination de la liste (index de la page, nombre d'éléments dans la page à retourner).
     * @return la liste paginée des informations recherchées.
     */
    Page<User> getAllManagedUsersByUsername(final String pUsername, final Pageable pPageable);

    /**
     * Obtenir la liste paginée des informations des utilisateurs n'ayant pas le login spécifié.
     * 
     * @param pUsername le login de l'utilisateur (le critère de recherche).
     * @param pPageable condition de pagination de la liste (index de la page, nombre d'éléments dans la page à retourner).
     * @return la liste paginée des informations recherchées.
     */
    Page<User> getAllManagedUsersByUsernameNot(final String pUsername, final Pageable pPageable);

    /**
     * Obtenir la liste paginée des informations des utilisateurs ayant le login spécifié.
     * 
     * @param pUsername le login de l'utilisateur (le critère de recherche).
     * @param pPageable condition de pagination de la liste (index de la page, nombre d'éléments dans la page à retourner).
     * @return la liste paginée des informations recherchées.
     */
    Page<User> getAllManagedUsersUsernameContains(final String pUsername, Pageable pPageable);

    /**
     * Obtenir la liste des utilisateurs activés.
     * 
     * @return la liste des utilisateurs activés.
     */
    Collection<User> getUsersByEnabledIsTrue();

    /**
     * Obtenir la liste des utilisateurs non désactivés.
     * 
     * @return la liste des utilisateurs désactivés.
     */
    Collection<User> getUsersByEnabledIsFalse();

    /**
     * Supprimer les informations d'un utiliateur à partir de son identifiant technique.
     * 
     * @param id identifiant technique de l'utilisateur à supprimer.
     */
    void deleteUserById(Long id);
}
