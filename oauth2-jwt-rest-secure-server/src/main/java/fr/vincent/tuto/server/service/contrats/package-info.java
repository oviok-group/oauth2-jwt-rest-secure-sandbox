/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 10 déc. 2020
 * Heure de création : 02:48:12
 * Package : fr.vincent.tuto.server.service.contrats
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les contrats de défintion de certaines fonctionnalités de 'application.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.service.contrats;
