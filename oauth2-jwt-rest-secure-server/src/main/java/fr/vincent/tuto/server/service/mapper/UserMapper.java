/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : UserMapper.java
 * Date de création : 9 déc. 2020
 * Heure de création : 08:26:23
 * Package : fr.vincent.tuto.server.service.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.service.mapper;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import fr.vincent.tuto.common.mapper.GenericObjectMapper;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.model.payload.UserDTO;
import fr.vincent.tuto.server.model.po.User;

/**
 * Service de conversion des objets {@link User} et objets {@link UserDTO} et vice cersa.
 * 
 * @author Vincent Otchoun
 */
@Service
public class UserMapper extends GenericObjectMapper<User, UserDTO> implements InitializingBean
{
    /**
     * Constructeur avec paramètres pour injection du beans en dépendances.
     * 
     * @param pModelMapper le bean de conversion des modèles selon le type.
     */
    protected UserMapper(ModelMapper pModelMapper)
    {
        super(pModelMapper);
    }

    //
    private static final String BEAN_NOT_NULL_MSG = "Le bean du mapper ne peut être null.";

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDTO toDestObject(final User pUser)
    {
        final UserDTO dto = this.modelMapper.map(pUser, UserDTO.class);

        // roles
        final Set<String> roles = Optional.ofNullable(pUser.getRoles())//
        .orElseGet(Collections::emptySet)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(Role::getAuthority)//
        .collect(Collectors.toSet())//
        ;
        dto.setRoles(roles);
        return dto;
    }

    @Override
    public User toSourceObject(UserDTO pUserDTO)
    {
        final User user = this.modelMapper.map(pUserDTO, User.class);

        // Chiffer le mot de passe avant la persistance
        final String newPassword = pUserDTO.getPassword();
        final String encryptedPassword = this.passwordEncoder.encode(newPassword);

        // roles
        final Set<Role> roles = Optional.ofNullable(pUserDTO.getRoles())//
        .orElseGet(Collections::emptySet)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(Role::valueOf)//
        .collect(Collectors.toSet())//
        ;
        // Affectaion des valeurs
        user.setPassword(encryptedPassword);
        user.setRoles(roles);
        return user;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(this.modelMapper, BEAN_NOT_NULL_MSG);
    }

}
