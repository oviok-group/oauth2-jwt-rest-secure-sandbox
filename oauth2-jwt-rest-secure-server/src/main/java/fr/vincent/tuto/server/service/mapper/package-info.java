/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 9 déc. 2020
 * Heure de création : 06:02:13
 * Package : fr.vincent.tuto.server.service.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Service de transformation ou conversion des objets.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.service.mapper;
