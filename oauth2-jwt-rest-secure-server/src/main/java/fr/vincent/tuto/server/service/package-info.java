/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : package-info.java
 * Date de création : 4 déc. 2020
 * Heure de création : 14:35:16
 * Package : fr.vincent.tuto.server.service
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Composants fournissant des services pour le bon fontionnement de l'application.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.server.service;
