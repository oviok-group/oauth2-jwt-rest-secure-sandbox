/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : CustomUserDetailsService.java
 * Date de création : 3 déc. 2020
 * Heure de création : 11:29:52
 * Package : fr.vincent.tuto.server.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server.service.security;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import fr.vincent.tuto.server.constant.ServerConstants;
import fr.vincent.tuto.server.dao.UserDAO;

/**
 * Composant/Service de personnalisation de {@link UserDetailsService} pour le chargement des données spécifiques de
 * l'utilisateur à partir de la base de données.
 * 
 * @author Vincent Otchoun
 */
@Component(value = "userDetailsService")
public class CustomUserDetailsService implements UserDetailsService
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserDAO userDAO;


    /**
     * {@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(final String pUsername) throws UsernameNotFoundException
    {
        LOGGER.debug("[loadUserByUsername] - Authentification de l'utilisateur avec : [{}]", pUsername);

        // Vérifier la validité du mail si la recherche est effectuée à partir du mail
        boolean isValidEmail = new EmailValidator().isValid(pUsername, null);
        if (isValidEmail)
        {
            return this.userDAO.findOneWithRolesByEmailIgnoreCase(pUsername)//
            .map(user -> ServerConstants.createSpringSecurityUser(pUsername, user))//
            .orElseThrow(() -> new UsernameNotFoundException(ServerConstants.MAIL_MSG + pUsername + ServerConstants.DB_MSG));
        }

        return this.userDAO.findOneWithRolesByUsernameIgnoreCase(pUsername)//
        .map(user -> ServerConstants.createSpringSecurityUser(pUsername, user))//
        .orElseThrow(() -> new UsernameNotFoundException(ServerConstants.USER_MSG + pUsername + ServerConstants.DB_MSG));

    }

    public UserDAO getUserDAO()
    {
        return this.userDAO;
    }
}
