/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : UserService.java
 * Date de création : 4 janv. 2021
 * Heure de création : 22:53:01
 * Package : fr.vincent.tuto.server.service.user
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.service.user;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import fr.vincent.tuto.server.dao.UserDAO;
import fr.vincent.tuto.server.model.po.User;
import fr.vincent.tuto.server.service.contrats.IUserService;

/**
 * Composant de gestion des informations des comptes des utilisateurs du système d'informations.
 * 
 * @author Vincent Otchoun
 */
@Service
public class UserService implements IUserService
{
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User createNewUser(User pUser)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public User updateUser(Long pId, User pUser)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void changePassword(String pCurrentPassword, String pNewPassword)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public Optional<User> findUserByUsername(String pUsername)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<User> findUserByEmailIgnoreCase(String pEmail)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean existsUserByUsername(String pUsername)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean existsUserByEmail(String pEmail)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<User> findUserById(Long id)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<User> findUserWithRolesById(Long pId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<User> findUserWithRolesByUsernameIgnoreCase(String pUsername)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<User> findUserWithRolesByEmailIgnoreCase(String pEmail)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<User> getAllManagedUsers(Pageable pPageable)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<User> getAllManagedUsersByUsername(String pUsername, Pageable pPageable)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<User> getAllManagedUsersByUsernameNot(String pUsername, Pageable pPageable)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<User> getAllManagedUsersUsernameContains(String pUsername, Pageable pPageable)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<User> getUsersByEnabledIsTrue()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<User> getUsersByEnabledIsFalse()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteUserById(Long id)
    {
        // TODO Auto-generated method stub

    }

}
