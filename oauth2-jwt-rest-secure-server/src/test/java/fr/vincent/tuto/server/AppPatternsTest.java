/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AppPatternsTest.java
 * Date de création : 4 déc. 2020
 * Heure de création : 10:19:13
 * Package : fr.vincent.tuto.server
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonBeansConfig;
import fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.constant.ServerConstants;

/**
 * Class de tests unitares des patterns définis.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "patternTest", classes = { AppRootConfig.class, CommonBeansConfig.class })
@ActiveProfiles("test")
@SpringBootTest
public class AppPatternsTest
{
    //
    private static final String MOT_DE_PASSE = "admin1_19511982";
    private static final String ENCRYPT_PWD = "$2a$12$t8u5rHr4fBCz4MjXLm.aiOo4Xh9TgpKfjXlr0jy1Kly.2PPCtFiDy";
    private static final String EMAIL = "admin1.test@live.fr";
    private static final String LOGIN_1 = "admin1.test@live.fr";
    private static final String LOGIN_2 = "admin1";

    @Autowired
    private CommonBeansConfig commonBeansConfig;

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.commonBeansConfig = null;
    }

    @Test
    public void test_EncryptPwd()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();
        final String encrypt = passwordEncoder.encode(MOT_DE_PASSE);

        // System.err.println(">>> Le mote de passe encrypté est : \n " + encrypt);

        assertThat(encrypt).isNotNull();
        assertThat(encrypt).isNotEmpty();
        assertThat(encrypt).contains("$12$");
        assertThat(encrypt.length()).isLessThanOrEqualTo(60);
        assertThat(Pattern.compile(ServerConstants.PASSWORD_REGEX).matcher(encrypt).matches()).isTrue();
    }

    @Test
    public void test_MatchPwd()
    {
        assertThat(ServerConstants.PASSWORD_PATTERN.matcher(ENCRYPT_PWD).matches()).isTrue();
    }

    @Test
    public void test_MatchEmptyPwd()
    {
        assertThat(ServerConstants.PASSWORD_PATTERN.matcher(StringUtils.EMPTY).matches()).isFalse();
    }

    @Test(expected = NullPointerException.class)
    public void test_MatchNullPwd()
    {
        assertThat(ServerConstants.PASSWORD_PATTERN.matcher(null).matches()).isFalse();
    }

    @Test
    public void test_MacthEmail()
    {
        assertThat(ServerConstants.EMAI_PATTERN.matcher(EMAIL).matches()).isTrue();
    }

    @Test
    public void test_MatcLogin1()
    {
        assertThat(ServerConstants.LOGIN_PATTERN.matcher(LOGIN_1).matches()).isTrue();
    }

    @Test
    public void test_MatcLogin2()
    {
        assertThat(ServerConstants.LOGIN_PATTERN.matcher(LOGIN_2).matches()).isTrue();
    }

    @Test
    public void test_UniqueId()
    {
        final String id = RandomUniqueIdUtil.generateUniqueId();

        assertThat(id).isNotNull();
        assertThat(id).isNotEmpty();
        assertThat(ServerConstants.UUID_PATTERN.matcher(id).matches()).isTrue();
    }

    @Test
    public void test_UUID()
    {
        String id = "2df7980a-2f28-423b-9624-e7f4dda77288";
        final UUID uuid = RandomUniqueIdUtil.generateUUID(id.getBytes());

        assertThat(uuid).isNotNull();
        assertThat(uuid.toString()).isNotNull();
        assertThat(ServerConstants.UUID_PATTERN.matcher(uuid.toString()).matches()).isTrue();
    }
}
