/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : CryptoTest.java
 * Date de création : 24 déc. 2020
 * Heure de création : 11:44:44
 * Package : fr.vincent.tuto.server
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.model.mo.CryptoModel;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil;
import fr.vincent.tuto.server.config.AppRootConfig;

/**
 * Test de récupération des éléments de cryptographie pour le serveur d'autorisation.
 * 
 * @author Vincent Otchoun
 */
@Ignore
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "cryptoTest", classes = { AppRootConfig.class, DatabasePropsService.class, ApplicationPropsService.class })
@SpringBootTest
@ActiveProfiles("test")
public class CryptoTest
{

    @Value("${vot.crypto-props.keystore-file-location}")
    private String keyStoreLocation;

    @Value("${vot.keystore.data.out.file}")
    private String outFilesPath;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void test()
    {
        final CryptoModel cryptoModel = new CryptoModel()//
        .aliasKeystore("my-user")//
        .keystorePassword("my-user")//
        .keyPassword("my-user");

        final KeyPair keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keyStoreLocation, cryptoModel);

        assertThat(keyPair).isNotNull();

        final RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        final String privateKeyStr = KeyPairProviderUtil.rsaPrivateKey(privateKey);

        System.err.println(">>>>>> Le contenu est : \n" + privateKeyStr);
        assertThat(privateKey).isNotNull();
    }

    @Test
    public void test_()
    {
        fail("Not yet implemented");
    }

    @Test
    public void testResources()
    {
        assertThat(this.keyStoreLocation).isNotNull();
        assertThat(this.outFilesPath).isNotNull();
    }

}
