/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ServerAppTestsUtil.java
 * Date de création : 24 nov. 2020
 * Heure de création : 22:48:17
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import fr.vincent.tuto.common.enumeration.BaseRolesEnum;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.model.payload.UserDTO;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Utilitaire commun pour les tests dans le module applicatif.
 * 
 * @author Vincent Otchoun
 */
public final class ServerAppTestsUtil
{
    //
    public static final String PWD_PATTERN = "$2a$12$";
    public static final String USER_ADMIN_USERNAME = "admin";
    public static final String USER_ADMIN_UPPER = "ADMIN";
    public static final String USER_ONE_USERNAME = "admin1";
    public static final String USER_ONE_EMAIL = "admin1.test@live.fr";
    public static final String USER_TWO_USERNAME = "admin2";
    public static final String USER_TWO_EMAIL = "admin2.test@live.fr";
    public static final String USER_THREE_USERNAME = "client1";
    public static final String USER_THREE_EMAIL = "client1.test@live.fr";

    public static final String ADMIN_EMAIL = "ADMIN.TEST@LIVE.FR";
    public static final String ADMIN_EMAIL_LOWER = "admin.test@live.fr";
    public static final String INACTIVE_USER_MAIL = "client3.test@live.fr";

    public static final String CLIENT_ID = "my-user-admin";
    public static final String API_TEST_URI = "rest/api/test";
    public static final String CLIENT_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsibXktdXNlci1yZXN0LWFwaSJdLCJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE2OTU1Njg1NjgsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwianRpIjoiZTU5NTRlNTItZDRmYS00MGYwLWFkODMtNTI4YmIzZWVhZmRmIiwiY2xpZW50X2lkIjoibXktdXNlci1hZG1pbiJ9.cAZ3YnO9zbT33oLR3VUeFzLctsOw8gVbJoMFM8NEZECIiVTdED1PfaidMR8xYMp6L5MXNZ-mWKVCcFPpeRvXNW9DUhFimXTjVCXyUDJtYwcGn4oNF2rmtZx7MCL3Ai4EoFhTLMWHAwvxhMfE38137raTXBGNAnkRPOfYyuRBIh_V-P8kfOUBRURwWZs21NczSRt3LMauce53l50yGzWMqL3SInqDACnlbnNAw9gAYDIlJRXtM8gqbf91BazWeun7ZtCI6vsDEcqaDUk1WWeHX8X9Pyhw9P0ze78-y0B4MsHe2GN6cF0tgxcqaRMYay4Rdk1NOAp68KW5fTpmR_VnovLo8kvUXM1VH_3PtWBrjW8qtIrhCABn_Syy-YW77-iKmvfn3RTVc0wyrGGmENE8Y-WM6-vckx766m4uepFdKAPrTt_yOSHxRlLZRMHwx0DL6nZMRCEmhiH2ne51Jw5hQyJ-ut4bnUACpmNqPSr_6IlqKSASUJfOU1vQFHrsvZeZEUyelJ0xPWST08CweWggAsPZjQA_ncYAdRuqD65CHzzwjdk4-7COeXY7VUpBhhjDE_WXoh7BvM7lnFPqwS41Js9Fju4NTkBtKFAbwl84R6bVfl9Tw9zkF7X8Z8MzPtLqgWGkf1MF7-DPtqozaqACnfn-CjtrxcMrqpWu7aSfjKc";

    //
    public static final String AUTH_TOKEN_PREFIX = "Bearer ";
    public static final String BASIC_AUTH_PREFIX = "Basic ";
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String PAYLOAD_DATA = "payload";
    public static final Long ONE_MINUTE = 60000L;
    public static final String ANONYMOUS_ROLE = "anonymous1";
    public static final String NOT_EXIST_ANONYMOUS_ROLE = "anonymous";

    public static final String REAL_BASE64_TEST_TOKEN = "87ZxNTF3hqIxFpKB0n06EeH5femEJ/vf7f/C2Ie7BOkSTV/GynzjR4WdOGKM5DZRi+ZMiSAy0XgDFlOS/nyTHw==";
    public static final String BAD_BASE64_TEST_TOKEN = "kuse9ecUZO7Blnpij6fPknsHufalY0J50go9YV5KzhapJvIsQ/L1Qyq9h1IYLqQzpOsOv3Oh1t/1BGbcsL3FKw==";
    public static final String AUTHORITIES_KEY = "auth";

    public static final String ADMIN = "admin";
    public static final String TOKEN = "token";
    public static final String ANONYMOUS = "anonymous";
    public static final String USER_MANAGER = "user_manager";
    public static final String MANAGER = "manager";
    public static final String MANAGER_UPPER = "MANAGER";
    public static final String CLIENT = "client";
    public static final String AUTH_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbm9ueW1vdXMiLCJleHAiOjE2MDM4NzgyMzF9.-V9sx5fnyHT7mY1B0e1vp1MY-OVEGW_DvOKOhD6wtB-HEZOa6Je3z1huJyJchD-iIRtaeaZKaL8jGAanoqrlMQ";

    /**
     * Constructeur
     */
    private ServerAppTestsUtil()
    {
    }

    /**
     * Création de jeton d'authentification non valide.
     * 
     * @param testKey la clé de signature u jeton d'authentification.
     * @return le jeton d'authentification.
     */
    public static String createUnsupportedToken(final Key testKey)
    {
        return Jwts.builder()//
        .setPayload(PAYLOAD_DATA)//
        .signWith(testKey, SignatureAlgorithm.HS512)//
        .compact();
    }

    /**
     * Construire le jeton d'authentification de l'utilisateur avec une clé encodée Base64.
     * 
     * @param pBase64SecretKey le mot de passe encodé Base64.
     * @param pUser            l'utilisateur courant.
     * @return le jeton d'authentification.
     */
    public static String createTestToken(final String pBase64SecretKey, final String pUser)
    {
        final Key otherKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(pBase64SecretKey));
        final Map<String, Object> header = Maps.newHashMap();
        header.put(Header.TYPE, Header.JWT_TYPE);

        return Jwts.builder()//
        .setId(UUID.randomUUID().toString())//
        .setIssuedAt(new Date())//
        .setSubject(pUser)//
        .setHeader(header)
        // .signWith(otherKey)//
        .signWith(otherKey, SignatureAlgorithm.HS512)//
        .setExpiration(new Date(new Date().getTime() + ONE_MINUTE))//
        .compact();
    }

    /**
     * Construire le jeton de la demande d'authentification.
     * 
     * @return le jeton de la demande d'authentification.
     */
    public static Authentication createAuthentication()
    {
        return new UsernamePasswordAuthenticationToken(ANONYMOUS_ROLE, "anonymous", Collections.singletonList(new SimpleGrantedAuthority(BaseRolesEnum.ANONYMOUS.getAuthority())));
    }

    /**
     * Construire le jeton de la demande d'authentification par recherche en abse de données des informations
     * d'authentification de l'utilisateur.
     *
     * @param pUserDetailsService
     * @return
     */
    public static Authentication createDataBaseAuthentication(final UserDetailsService pUserDetailsService)
    {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));

        final UserDetails springSecurityUser = pUserDetailsService.loadUserByUsername(BaseRolesEnum.ANONYMOUS.getAuthority());
        return createAuthentication((User) springSecurityUser, authorities);
    }

    /**
     * @param pUser
     * @return
     */
    public static Authentication createAuthentication(final User pUser)
    {
        return new UsernamePasswordAuthenticationToken(pUser, pUser.getPassword(), pUser.getAuthorities());
    }

    /**
     * Construire le jeton de la demande d'authentification avec les infomrations d'identification de l'utilisateur.
     * 
     * @param pPrincipal   l'utilisateur à authentifier.
     * @param pCredentials les informations d'identification.
     * @return le jeton de la demande d'authentification.
     */
    public static UsernamePasswordAuthenticationToken createAuthentication(final String pPrincipal, final String pCredentials)
    {
        return new UsernamePasswordAuthenticationToken(pPrincipal, pCredentials);
    }

    /**
     * Construire le jeton de la demande d'authentification avec les infomrations d'identification de l'utilisateur.
     * 
     * @param pPrincipal   l'utilisateur à authentifier.
     * @param pCredentials les informations d'identification.
     * @param pAuthorities les rôles de l'utilisateur.
     * @return le jeton de la demande d'authentification.
     */
    public static UsernamePasswordAuthenticationToken creatieAuthentication(final String pPrincipal, final String pCredentials, final Collection<GrantedAuthority> pAuthorities)
    {
        return new UsernamePasswordAuthenticationToken(pPrincipal, pCredentials, pAuthorities);
    }

    /**
     * @param pUser
     * @param pAuthorities
     * @return
     */
    public static UsernamePasswordAuthenticationToken createAuthentication(final User pUser, final Collection<GrantedAuthority> pAuthorities)
    {
        return new UsernamePasswordAuthenticationToken(pUser, pUser.getPassword(), pAuthorities);
    }

    /**
     * Construire un utilisateur au sens Spring Security avec les informations d'identitifcation.
     * 
     * @param pUsername    le login de l'utilisateur.
     * @param pPassword    le mot de passe de l'utilisateur.
     * @param pAuthorities les rôles de l'utilisateur.
     * @return
     */
    public static User createSpringUser(final String pUsername, final String pPassword, final Collection<GrantedAuthority> pAuthorities)
    {
        return new User(pUsername, pPassword, pAuthorities);
    }

    /**
     * Construire une autorité accordée à un objet d'authentification.
     * 
     * @param pRole le rôle associé.
     * @return
     */
    public static SimpleGrantedAuthority createRole(final String pRole)
    {
        return new SimpleGrantedAuthority(pRole);
    }

    /**
     * Fournir le jeu de données pour intialisser la base de données pour les tests unitaires et d'intégration de façon
     * programmatique.
     * 
     * @return le jeu de données.
     */
    public static List<fr.vincent.tuto.server.model.po.User> creerJeuDeDonnees()
    {
        final List<fr.vincent.tuto.server.model.po.User> users = Lists.newArrayList();
        // ADMIN
        final fr.vincent.tuto.server.model.po.User admin = createActiveUser(Role.ROLE_ADMIN.getAuthority(), "admin", "admin_19511982#", "admin.test@live.fr");

        // CLIENT
        final fr.vincent.tuto.server.model.po.User client = createActiveUser(Role.ROLE_USER.getAuthority(), "client", "client_19511982#", "client.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client1 = createActiveUser(Role.ROLE_USER.getAuthority(), "client1", "client1_19511982#", "client1.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client2 = createActiveUser(Role.ROLE_USER.getAuthority(), "client2", "client2_19511982#", "client2.test@live.fr");

        // USER MANAGER
        final fr.vincent.tuto.server.model.po.User manager = createActiveUser("USER_MANAGER", "manager", "manager_19511982#", "manager.test@live.fr");

        // CLIENT INACTIF
        final fr.vincent.tuto.server.model.po.User client3 = createInActiveUser(Role.ROLE_USER.getAuthority(), "client3", "client3_19511982#", "client3.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client4 = createInActiveUser(Role.ROLE_USER.getAuthority(), "client4", "client4_19511982#", "client4.test@live.fr");
        final fr.vincent.tuto.server.model.po.User client5 = createInActiveUser(Role.ROLE_USER.getAuthority(), "client5", "client5_19511982#", "client5.test@live.fr");

        users.add(admin);
        users.add(client);
        users.add(client1);
        users.add(client2);

        users.add(manager);

        users.add(client3);
        users.add(client4);
        users.add(client5);
        return users;
    }

    /**
     * @param pRole
     * @param pUsername
     * @param pPwd
     * @param pEmail
     * @return
     */
    public static fr.vincent.tuto.server.model.po.User createActiveUser(final String pRole, final String pUsername, final String pPwd, final String pEmail)
    {
        // ADMIN
        final Set<Role> adminRoles = new HashSet<>();
        adminRoles.add(Role.valueOf(pRole));
        final fr.vincent.tuto.server.model.po.User adminUser = new fr.vincent.tuto.server.model.po.User()//
        .id(1L)//
        .username(pUsername)//
        .password(pwdEncoder().encode(pPwd))//
        .email(pEmail)//
        .accountExpired(Boolean.FALSE)//
        .accountLocked(Boolean.FALSE)//
        .credentialsExpired(Boolean.FALSE)//
        .enabled(Boolean.TRUE)//
        .roles(adminRoles)//
        .createdTime(LocalDateTime.now(ZoneId.systemDefault()))//
        .updatedTime(LocalDateTime.now(ZoneId.systemDefault()));
        adminUser.setVersion(Integer.valueOf(0));
        return adminUser;
    }

    /**
     * @param pRole
     * @param pUsername
     * @param pPwd
     * @param pEmail
     * @return
     */
    public static fr.vincent.tuto.server.model.po.User createInActiveUser(final String pRole, final String pUsername, final String pPwd, final String pEmail)
    {
        // ACTIVE USER
        final Set<Role> activeUserRoles = new HashSet<>();
        activeUserRoles.add(Role.valueOf(pRole));
        final fr.vincent.tuto.server.model.po.User activeUser = new fr.vincent.tuto.server.model.po.User()//
        .id(1L)//
        .username(pUsername)//
        .password(pwdEncoder().encode(pPwd))//
        .email(pEmail)//
        .accountExpired(Boolean.TRUE)//
        .accountLocked(Boolean.TRUE)//
        .credentialsExpired(Boolean.TRUE)//
        .enabled(Boolean.FALSE)//
        .roles(activeUserRoles)//
        .createdTime(LocalDateTime.now(ZoneId.systemDefault()))//
        .updatedTime(LocalDateTime.now(ZoneId.systemDefault()));
        activeUser.setVersion(Integer.valueOf(0));
        return activeUser;
    }

    /**
     * @return
     */
    public static UserDTO createDTO()
    {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setUsername("dtoUser");
        userDTO.setPassword(pwdEncoder().encode("dtoUser_19821951#"));
        userDTO.setEmail("dtoUser.test@live.fr");
        userDTO.setAccountExpired(Boolean.FALSE);
        userDTO.setAccountLocked(Boolean.FALSE);

        final Set<String> rolesStr = new HashSet<>();
        rolesStr.add(Role.ROLE_USER.getAuthority());
        userDTO.setRoles(rolesStr);
        return userDTO;
    }

    private static BCryptPasswordEncoder pwdEncoder()
    {
        return new BCryptPasswordEncoder(12);
    }
}
