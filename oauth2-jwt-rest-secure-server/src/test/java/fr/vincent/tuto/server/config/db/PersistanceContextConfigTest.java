/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : PersistanceContextConfigTest.java
 * Date de création : 1 déc. 2020
 * Heure de création : 11:02:08
 * Package : fr.vincent.tuto.server.config.db
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.db;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.SharedEntityManagerBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariDataSource;

import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.RestSecureServerApplicationStarter;
import fr.vincent.tuto.server.config.AppRootConfig;

/**
 * Classe des Tests Unitaires des objets de type {@link PersistanceContextConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "persistanceContextConfigTest", classes = { AppRootConfig.class, DatabasePropsService.class, PersistanceContextConfig.class })
@SpringBootTest(classes = RestSecureServerApplicationStarter.class)
@ActiveProfiles("test")
public class PersistanceContextConfigTest
{
    private static final String DRIVER_CLASS_NAME = "org.h2.Driver";
    private static final String PERSIT_UNIT_NAME = "OAuth2JwtRestSecureServerPUTest";

    @Autowired
    private PersistanceContextConfig persistanceContextConfig;

    @Autowired
    private DatabasePropsService databasePropsService;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.persistanceContextConfig.setDatabasePropsService(this.databasePropsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.persistanceContextConfig = null;
        this.databasePropsService = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#dataSourceProperties()}.
     */
    @Test
    public void testDataSourceProperties()
    {
        DataSourceProperties dataSourceProperties = this.persistanceContextConfig.dataSourceProperties();

        assertThat(dataSourceProperties).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#dataSource()}.
     */
    @Test
    public void testDataSource()
    {
        final HikariDataSource dataSource = this.persistanceContextConfig.dataSource();

        assertThat(dataSource).isNotNull();
        assertThat(dataSource.getClass()).isSameAs(HikariDataSource.class);
        assertThat(dataSource.getConnectionTimeout()).isGreaterThan(0);
        assertThat(dataSource.getDriverClassName()).isEqualTo(DRIVER_CLASS_NAME);
        assertThat(dataSource.getIdleTimeout()).isGreaterThan(0);
    }

    @Test(expected = NullPointerException.class)
    public void testDataSource_ShouldThrowException()
    {
        PersistanceContextConfig config = new PersistanceContextConfig();
        config.setDatabasePropsService(null);
        final HikariDataSource dataSource = config.dataSource();

        assertThat(dataSource).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#entityManagerFactory()}.
     */
    @Test
    public void testEntityManagerFactory()
    {
        final LocalContainerEntityManagerFactoryBean bean = this.persistanceContextConfig.entityManagerFactory();

        assertThat(bean).isNotNull();
        assertThat(bean.getDataSource()).isNotNull();
        assertThat(bean.getDataSource()).isExactlyInstanceOf(HikariDataSource.class);
        assertThat(bean.getJpaDialect()).isExactlyInstanceOf(HibernateJpaDialect.class);
        assertThat(bean.getJpaVendorAdapter()).isExactlyInstanceOf(HibernateJpaVendorAdapter.class);
        assertThat(bean.getPersistenceUnitName()).isEqualTo(PERSIT_UNIT_NAME);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#sharedEntityManager()}.
     */
    @Test
    public void testSharedEntityManager()
    {
        final SharedEntityManagerBean sharedEntity = this.persistanceContextConfig.sharedEntityManager();

        assertThat(sharedEntity).isNotNull();
        assertThat(sharedEntity.getEntityManagerFactory()).isNotNull();
        assertThat(sharedEntity.getPersistenceUnitName()).isNull();
        assertThat(sharedEntity.getObject()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#transactionManager()}.
     */
    @Test
    public void testTransactionManager()
    {
        final PlatformTransactionManager transactionManager = this.persistanceContextConfig.transactionManager();

        assertThat(transactionManager).isNotNull();
        assertThat(transactionManager).isExactlyInstanceOf(JpaTransactionManager.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#hibernatJpaVendorAdapter()}.
     */
    @Test
    public void testHibernatJpaVendorAdapter()
    {
        final JpaVendorAdapter vendorAdapter = this.persistanceContextConfig.hibernatJpaVendorAdapter();

        assertThat(vendorAdapter).isNotNull();
        assertThat(vendorAdapter).isExactlyInstanceOf(HibernateJpaVendorAdapter.class);
        assertThat(vendorAdapter.getJpaDialect()).isNotNull();
        assertThat(vendorAdapter.getJpaPropertyMap()).isNotEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#hibernatJpaDialect()}.
     */
    @Test
    public void testHibernatJpaDialect()
    {
        final JpaDialect dialect = this.persistanceContextConfig.hibernatJpaDialect();

        assertThat(dialect).isNotNull();
        assertThat(dialect).isExactlyInstanceOf(HibernateJpaDialect.class);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#exceptionTranslationPostProcessor()}.
     */
    @Test
    public void testExceptionTranslationPostProcessor()
    {
        final PersistenceExceptionTranslationPostProcessor processor = this.persistanceContextConfig.exceptionTranslationPostProcessor();

        assertThat(processor).isNotNull();
        assertThat(processor).isExactlyInstanceOf(PersistenceExceptionTranslationPostProcessor.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#sqlExceptionTranslator()}.
     */
    @Test
    public void testSqlExceptionTranslator()
    {
        final SQLErrorCodeSQLExceptionTranslator codeSQLExceptionTranslator = this.persistanceContextConfig.sqlExceptionTranslator();

        assertThat(codeSQLExceptionTranslator).isNotNull();
        assertThat(codeSQLExceptionTranslator).isExactlyInstanceOf(SQLErrorCodeSQLExceptionTranslator.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.db.PersistanceContextConfig#getDatabasePropsService()}.
     */
    @Test
    public void testGetDatabasePropsService()
    {
        assertThat(this.persistanceContextConfig).isNotNull();
        assertThat(this.persistanceContextConfig.getDatabasePropsService()).isNotNull();
    }
}
