/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AuthServerOAuth2ConfigTest.java
 * Date de création : 28 déc. 2020
 * Heure de création : 13:44:06
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server.config.security;

import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.config.AppRootConfig;

/**
 * Classe des Tests Unitaires des objets de type {@link AuthServerOAuth2Config}.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "authServerOAuth2ConfigTest", classes = { AppRootConfig.class, DatabasePropsService.class, ApplicationPropsService.class,
        AuthServerOAuth2Config.class })
@SpringBootTest
@ActiveProfiles("test")
public class AuthServerOAuth2ConfigTest
{

    @Autowired
    private AuthServerOAuth2Config authServerOAuth2Config;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.AuthServerOAuth2Config#configure(org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer)}.
     * 
     * @throws Exception
     */
    @Test(expected = NullPointerException.class)
    public void testConfigureClientDetailsServiceConfigurer() throws Exception
    {
        ClientDetailsServiceConfigurer configurer = mock(ClientDetailsServiceConfigurer.class);
        this.authServerOAuth2Config.configure(configurer);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.AuthServerOAuth2Config#configure(org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer)}.
     * 
     * @throws Exception
     */
    @Test
    public void testConfigureAuthorizationServerSecurityConfigurer() throws Exception
    {
        AuthorizationServerSecurityConfigurer configurer = new AuthorizationServerSecurityConfigurer();
        this.authServerOAuth2Config.configure(configurer);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.AuthServerOAuth2Config#configure(org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer)}.
     * 
     * @throws Exception
     */
    @Test
    public void testConfigureAuthorizationServerEndpointsConfigurer() throws Exception
    {
        AuthorizationServerEndpointsConfigurer configurer = new AuthorizationServerEndpointsConfigurer();
        this.authServerOAuth2Config.configure(configurer);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.AuthServerOAuth2Config#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.authServerOAuth2Config.afterPropertiesSet();
    }
}
