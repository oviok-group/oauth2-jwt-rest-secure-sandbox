/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : ServerSecurityConfigTest.java
 * Date de création : 28 déc. 2020
 * Heure de création : 12:07:49
 * Package : fr.vincent.tuto.server.config.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.config.security;

import static org.mockito.Mockito.mock;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collection;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.ServerAppTestsUtil;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.config.db.PersistanceContextConfig;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.service.security.CustomUserDetailsService;

/**
 * Classe des Tests Unitaires des objets de type {{@link ServerSecurityConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "serverSecurityConfigTest", classes = { AppRootConfig.class, DatabasePropsService.class, PersistanceContextConfig.class,
        CustomUserDetailsService.class, ServerSecurityConfig.class, FilterChainProxy.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ServerSecurityConfigTest
{
    private static final String URL_TEST = "http://localhost:8500/oauth2-jwt-rest-secure-server/actuator/";

    @Autowired
    private ServerSecurityConfig serverSecurityConfig;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;
    private AuthenticationManagerBuilder managerBuilder;
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.mockMvc = MockMvcBuilders.//
        webAppContextSetup(this.context)//
        .apply(springSecurity())//
        .build();

        this.managerBuilder = mock(AuthenticationManagerBuilder.class);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#tokenStore()}.
     */
    @Test
    public void testTokenStore()
    {
        TokenStore tokenStore = this.serverSecurityConfig.tokenStore();

        Assertions.assertThat(tokenStore).isNotNull();
        Assertions.assertThat(tokenStore.findTokensByClientId("my-user")).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#approvalStore()}.
     */
    @Test
    public void testApprovalStore()
    {
        final ApprovalStore approvalStore = this.serverSecurityConfig.approvalStore();

        Assertions.assertThat(approvalStore).isNotNull();
    }


    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#accessTokenConverter()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAccessTokenConverter() throws Exception
    {
        final JwtAccessTokenConverter accessTokenConverter = this.serverSecurityConfig.accessTokenConverter();

        Assertions.assertThat(accessTokenConverter).isNotNull();
        Assertions.assertThat(accessTokenConverter.getKey()).isNotEmpty();

        Assertions.assertThat(accessTokenConverter.getKey().get("alg")).isNotEmpty();
        Assertions.assertThat(accessTokenConverter.getKey().get("alg")).isEqualTo("SHA256withRSA");
        Assertions.assertThat(accessTokenConverter.getKey().get("value")).isNotEmpty();
        Assertions.assertThat(accessTokenConverter.getKey().get("value")).contains("-----BEGIN PUBLIC KEY-----");
    }


    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#tokenServices()}.
     * 
     * @throws Exception
     */
    @Test
    public void testTokenServices() throws Exception
    {
        DefaultTokenServices tokenServices = this.serverSecurityConfig.tokenServices();

        Assertions.assertThat(tokenServices).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testTokenServices_ShouldThrowException() throws Exception
    {
        DefaultTokenServices tokenServices = this.serverSecurityConfig.tokenServices();

        Assertions.assertThat(tokenServices).isNotNull();
        Assertions.assertThat(tokenServices.getAccessToken(null)).isNotNull();

    }


    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#accessDeniedHandler()}.
     */
    @Test
    public void testAccessDeniedHandler()
    {
        OAuth2AccessDeniedHandler accessDeniedHandler = this.serverSecurityConfig.accessDeniedHandler();
        Assertions.assertThat(accessDeniedHandler).isNotNull();
    }


    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#authenticationProvider()}.
     */
    @Test(expected = BadCredentialsException.class)
    public void testAuthenticationProvider()
    {
        DaoAuthenticationProvider authenticationProvider = this.serverSecurityConfig.authenticationProvider();

        final SecurityContext context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(ServerAppTestsUtil.createRole(Role.ROLE_ANONYMOUS.getAuthority()));
        final User user = ServerAppTestsUtil.createSpringUser(ServerAppTestsUtil.ANONYMOUS, ServerAppTestsUtil.ANONYMOUS, authorities);
        final Authentication authenticationToken = ServerAppTestsUtil.createAuthentication(user, authorities);

        context.setAuthentication(authenticationToken);
        SecurityContextHolder.setContext(context);

        Assertions.assertThat(authenticationProvider).isNotNull();
        Assertions.assertThat(authenticationProvider.authenticate(authenticationToken)).isNotNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAuthenticationProvider_ShouldThrowException()
    {
        DaoAuthenticationProvider authenticationProvider = this.serverSecurityConfig.authenticationProvider();

        Assertions.assertThat(authenticationProvider).isNotNull();
        Assertions.assertThat(authenticationProvider.authenticate(null)).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#authenticationManager()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAuthenticationManager() throws Exception
    {

        final SecurityContext context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(ServerAppTestsUtil.createRole(Role.ROLE_USER_MANAGER.getAuthority()));
        final User user = ServerAppTestsUtil.createSpringUser(ServerAppTestsUtil.USER_MANAGER, ServerAppTestsUtil.USER_MANAGER, authorities);
        final Authentication authenticationToken = ServerAppTestsUtil.createAuthentication(user, authorities);

        context.setAuthentication(authenticationToken);
        SecurityContextHolder.setContext(context);

        AuthenticationManager authenticationManager = this.serverSecurityConfig.authenticationManager();

        Assertions.assertThat(authenticationManager).isNotNull();
    }

    @Test(expected = BadCredentialsException.class)
    public void testAuthenticationManager_BadCredentials() throws Exception
    {

        final SecurityContext context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(ServerAppTestsUtil.createRole(Role.ROLE_ANONYMOUS.getAuthority()));
        final User user = ServerAppTestsUtil.createSpringUser(ServerAppTestsUtil.ANONYMOUS, ServerAppTestsUtil.ANONYMOUS, authorities);
        final Authentication authenticationToken = ServerAppTestsUtil.createAuthentication(user, authorities);

        context.setAuthentication(authenticationToken);
        SecurityContextHolder.setContext(context);

        AuthenticationManager authenticationManager = this.serverSecurityConfig.authenticationManager();

        Assertions.assertThat(authenticationManager).isNotNull();
        Assertions.assertThat(authenticationManager.authenticate(authenticationToken)).isNotNull();
        Assertions.assertThat(authenticationManager.authenticate(authenticationToken).getName()).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testAuthenticationManager_ShouldCatchException() throws Exception
    {
        AuthenticationManager authenticationManager = this.serverSecurityConfig.authenticationManager();

        Assertions.assertThat(authenticationManager).isNotNull();
        Assertions.assertThat(authenticationManager.authenticate(null)).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#configure(org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder)}.
     * 
     * @throws Exception
     */
    @Test(expected = NullPointerException.class)
    public void testConfigureAuthenticationManagerBuilder() throws Exception
    {
        this.serverSecurityConfig.configure(this.managerBuilder);

        Assertions.assertThat(this.managerBuilder).isNotNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfigureAuthenticationManagerBuilder_Null() throws Exception
    {
        AuthenticationManagerBuilder managerBuilder = new AuthenticationManagerBuilder(null);
        this.serverSecurityConfig.configure(managerBuilder);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)}.
     * 
     * @throws Exception
     */
    @Test
    public void testConfigureHttpSecurity() throws Exception
    {
        this.mockMvc.perform(get(URL_TEST)).andExpect(status().is4xxClientError());
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#configure(org.springframework.security.config.annotation.web.builders.WebSecurity)}.
     * 
     * @throws Exception
     */
    @Test
    public void testConfigureWebSecurity() throws Exception
    {
        this.mockMvc.perform(get(URL_TEST)).andExpect(status().is4xxClientError());
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.config.security.ServerSecurityConfig#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.serverSecurityConfig.afterPropertiesSet();
    }

    @Test
    public void testResources()
    {
        Assertions.assertThat(this.serverSecurityConfig).isNotNull();
        Assertions.assertThat(this.mockMvc).isNotNull();
        Assertions.assertThat(this.context).isNotNull();
    }

}
