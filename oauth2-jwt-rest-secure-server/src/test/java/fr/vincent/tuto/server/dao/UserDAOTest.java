/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : UserDAOTest.java
 * Date de création : 10 déc. 2020
 * Heure de création : 02:52:40
 * Package : fr.vincent.tuto.server.dao
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.ServerAppTestsUtil;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.config.db.PersistanceContextConfig;
import fr.vincent.tuto.server.model.po.User;

/**
 * Classe des Tests Unitaires des objets de type {@link UserDAO}.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "userDAOTest", classes = { AppRootConfig.class, DatabasePropsService.class, PersistanceContextConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class UserDAOTest
{
    //
    @Autowired
    private UserDAO userDAO;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        // this.initData();
    }


    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findOneByUsername(java.lang.String)}.
     */
    @Test
    public void testFindOneByUsername()
    {
        final Optional<User> optional = this.userDAO.findOneByUsername(ServerAppTestsUtil.ADMIN);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.isEmpty()).isFalse();
    }

    @Test
    public void testFindOneByUsername_WithEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneByUsername(StringUtils.EMPTY);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    @Test
    public void testFindOneByUsername_WithNull()
    {
        final Optional<User> optional = this.userDAO.findOneByUsername(null);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findOneByEmailIgnoreCase(java.lang.String)}.
     */
    @Test
    public void testFindOneByEmailIgnoreCase()
    {
        final Optional<User> optional = this.userDAO.findOneByUsername(ServerAppTestsUtil.ADMIN_EMAIL);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    @Test
    public void testFindOneByEmailIgnoreCase_WithEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneByEmailIgnoreCase(StringUtils.EMPTY);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    @Test
    public void testFindOneByEmailIgnoreCase_WithNull()
    {
        final Optional<User> optional = this.userDAO.findOneByEmailIgnoreCase(null);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#existsByUsername(java.lang.String)}.
     */
    @Test
    public void testExistsByUsername()
    {
        final Boolean isExist = this.userDAO.existsByEmail(ServerAppTestsUtil.MANAGER);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isFalse();
    }

    @Test
    public void testExistsByUsername_WithEmpty()
    {
        final Boolean isExist = this.userDAO.existsByUsername(StringUtils.EMPTY);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isFalse();
    }

    @Test
    public void testExistsByUsername_WithNull()
    {
        final Boolean isExist = this.userDAO.existsByUsername(null);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#existsByEmail(java.lang.String)}.
     */
    @Test
    public void testExistsByEmail()
    {
        final Boolean isExist = this.userDAO.existsByEmail(ServerAppTestsUtil.ADMIN_EMAIL_LOWER);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isTrue();
    }

    @Test
    public void testExistsByEmail_WithEmpty()
    {
        final Boolean isExist = this.userDAO.existsByEmail(StringUtils.EMPTY);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isFalse();
    }

    @Test
    public void testExistsByEmail_WithNull()
    {
        final Boolean isExist = this.userDAO.existsByEmail(null);

        assertThat(isExist).isNotNull();
        assertThat(isExist).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findOneWithRolesById(java.lang.Long)}.
     */
    @Test
    public void testFindOneWithRolesById()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesById(2L);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.isEmpty()).isFalse();
    }

    @Test
    public void testFindOneWithRolesById_ShouldBeEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesById(null);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findOneWithRolesByUsernameIgnoreCase(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByUsernameIgnoreCase()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByUsernameIgnoreCase(ServerAppTestsUtil.MANAGER);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.isEmpty()).isFalse();
    }

    @Test
    public void testFindOneWithRolesByUsernameIgnoreCase_WithUpperCase()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByUsernameIgnoreCase(ServerAppTestsUtil.MANAGER_UPPER);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.isEmpty()).isFalse();
    }

    @Test
    public void testFindOneWithRolesByUsernameIgnoreCase_WithEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByUsernameIgnoreCase(StringUtils.EMPTY);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    @Test
    public void testFindOneWithRolesByUsernameIgnoreCase_ShouldBeEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByUsernameIgnoreCase(null);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findOneWithRolesByEmailIgnoreCase(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByEmailIgnoreCase()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByEmailIgnoreCase(ServerAppTestsUtil.ADMIN_EMAIL);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.isEmpty()).isFalse();
    }

    @Test
    public void testFindOneWithRolesByEmailIgnoreCase_WithEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByEmailIgnoreCase(StringUtils.EMPTY);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    @Test
    public void testFindOneWithRolesByEmailIgnoreCase_ShouldBeEmpty()
    {
        final Optional<User> optional = this.userDAO.findOneWithRolesByEmailIgnoreCase(null);

        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
        assertThat(optional.isEmpty()).isTrue();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.dao.UserDAO#findAllByUsername(java.lang.String, org.springframework.data.domain.Pageable)}.
     */
    @Test
    public void testFindAllByUsernameStringPageable()
    {
        int pageNumber = 0; // zero-based page index, must NOT be negative.
        int pageSize = 5; // number of items in a page to be returned, must be greater than 0.
        Pageable paging = PageRequest.of(pageNumber, pageSize);

        final Page<User> users = this.userDAO.findAllByUsername(ServerAppTestsUtil.MANAGER, paging);

        assertThat(users).isNotNull();
        assertThat(users.getContent()).isNotEmpty();
        assertThat(users.getContent().size()).isEqualTo(1);
    }

    @Test
    public void testFindAllByUsernameStringPageable_WithEmpty()
    {
        int pageNumber = 0; // zero-based page index, must NOT be negative.
        int pageSize = 5; // number of items in a page to be returned, must be greater than 0.
        Pageable paging = PageRequest.of(pageNumber, pageSize);

        final Page<User> users = this.userDAO.findAllByUsername(StringUtils.EMPTY, paging);

        assertThat(users).isNotNull();
        assertThat(users.getContent()).isEmpty();
        assertThat(users.getContent().size()).isEqualTo(0);
    }

    @Test
    public void testFindAllByUsernameStringPageable_ShouldBeEmpty()
    {
        final Page<User> users = this.userDAO.findAllByUsername(null, null);

        assertThat(users).isNotNull();
        assertThat(users.getContent()).isEmpty();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.dao.UserDAO#findAllByUsernameNot(java.lang.String, org.springframework.data.domain.Pageable)}.
     */
    @Test
    public void testFindAllByUsernameNot()
    {
        int pageNumber = 0; // zero-based page index, must NOT be negative.
        int pageSize = 5; // number of items in a page to be returned, must be greater than 0.
        Pageable paging = PageRequest.of(pageNumber, pageSize);

        final Page<User> user = this.userDAO.findAllByUsernameNot(ServerAppTestsUtil.ANONYMOUS, paging);

        assertThat(user).isNotNull();
        assertThat(user.getContent()).isNotEmpty();
        assertThat(user.getContent().size()).isEqualTo(5);
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testFindAllByUsernameNot_ShouldThrowException()
    {
        final Page<User> user = this.userDAO.findAllByUsernameNot(null, null);

        assertThat(user).isNull();
        assertThat(user.getContent()).isEmpty();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.dao.UserDAO#findByUsernameContains(java.lang.String, org.springframework.data.domain.Pageable)}.
     */
    @Test
    public void testFindByUsernameContains()
    {
        int pageNumber = 0; // zero-based page index, must NOT be negative.
        int pageSize = 5; // number of items in a page to be returned, must be greater than 0.
        Pageable paging = PageRequest.of(pageNumber, pageSize);

        final Page<User> user = this.userDAO.findByUsernameContains(ServerAppTestsUtil.USER_ADMIN_USERNAME, paging);

        assertThat(user).isNotNull();
        assertThat(user.getContent()).isNotEmpty();
        assertThat(user.getContent().size()).isEqualTo(1);
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testFindByUsernameContains_ShouldThrowException()
    {
        final Page<User> user = this.userDAO.findByUsernameContains(null, null);

        assertThat(user).isNotNull();
        assertThat(user.getContent()).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findAllByUsername(java.lang.String)}.
     */
    @Test
    public void testFindAllByUsernameString()
    {
        final List<User> users = (List<User>) this.userDAO.findAllByUsername(ServerAppTestsUtil.ADMIN);

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(1);
    }

    @Test
    public void testFindAllByUsernameString_ShouldThrowException()
    {
        final List<User> users = (List<User>) this.userDAO.findAllByUsername(null);

        assertThat(users).isEmpty();
        assertThat(users.size()).isEqualTo(0);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findAllByEnabledIsTrue()}.
     */
    @Test
    public void testFindAllByEnabledIsTrue()
    {
        final List<User> users = (List<User>) this.userDAO.findAllByEnabledIsTrue();

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(5);
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.dao.UserDAO#findAllByEnabledIsFalse()}.
     */
    @Test
    public void testFindAllByEnabledIsFalse()
    {
        final List<User> users = (List<User>) this.userDAO.findAllByEnabledIsFalse();

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(3);
    }

    @SuppressWarnings("unused")
    private void initData()
    {
        ServerAppTestsUtil.creerJeuDeDonnees()//
        .forEach(user -> this.userDAO.save(user));
    }

}
