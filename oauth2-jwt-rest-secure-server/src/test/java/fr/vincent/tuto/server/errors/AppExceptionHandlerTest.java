/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : AppExceptionHandlerTest.java
 * Date de création : 11 déc. 2020
 * Heure de création : 20:45:15
 * Package : fr.vincent.tuto.server.errors
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.errors;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Method;
import java.util.Set;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.MethodParameter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.mock.http.MockHttpInputMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.collect.Sets;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.error.ApiResponseError;
import fr.vincent.tuto.common.model.payload.GenericApiResponse;
import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.config.db.PersistanceContextConfig;
import fr.vincent.tuto.server.model.po.User;

/**
 * Classe des Tests Unitaires des objets de type {@link AppExceptionHandler}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "appExceptionHandlerTest", classes = { AppRootConfig.class, DatabasePropsService.class, PersistanceContextConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class AppExceptionHandlerTest
{
    @Autowired
    private AppExceptionHandler<User> appExceptionHandler;

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.appExceptionHandler = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleHttpClientErrorException(org.springframework.web.client.HttpClientErrorException)}.
     */
    @Test
    public void testHandleHttpClientErrorException()
    {
        final HttpClientErrorException clientErrorException = new HttpClientErrorException(HttpStatus.UNAUTHORIZED, AppConstants.ACCESS_DENIED);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpClientErrorException(clientErrorException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.HTTP_CLIENT_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(clientErrorException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleHttpClientErrorException_BadRequest()
    {
        final HttpClientErrorException clientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, AppConstants.HTTP_CLIENT_ERROR);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpClientErrorException(clientErrorException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.HTTP_CLIENT_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(clientErrorException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleHttpClientErrorException_ShouldTrowNPE()
    {
        final HttpClientErrorException clientErrorException = new HttpClientErrorException(null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpClientErrorException(clientErrorException);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleHttpCustomAppException(fr.vincent.tuto.common.exception.CustomAppException)}.
     */
    @Test
    public void testHandleHttpCustomAppException()
    {
        final CustomAppException customAppException = new CustomAppException("Erreur customisée");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpCustomAppException(customAppException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.SERVER_INTERNAL_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(customAppException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleHttpCustomAppException_WithNull()
    {
        final CustomAppException customAppException = new CustomAppException(null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpCustomAppException(customAppException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.SERVER_INTERNAL_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(customAppException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleHttpCustomAppException_ShouldTrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleHttpCustomAppException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleNotReadableException(java.lang.Exception)}.
     */
    @Test
    public void testHandleNotReadableException()
    {
        MockHttpInputMessage inputMessage = new MockHttpInputMessage("mockInput".getBytes());
        final HttpMessageNotReadableException notReadableException = new HttpMessageNotReadableException("HttpMessageNotReadableExceptione", null, inputMessage);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotReadableException(notReadableException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.FORMAT_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(notReadableException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleNotReadableException_Json()
    {
        final JsonParseException jsonParseException = new JsonParseException(null, "JSON");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotReadableException(jsonParseException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.FORMAT_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(jsonParseException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleNotReadableException_WithNull()
    {
        final HttpMessageNotReadableException notReadableException = new HttpMessageNotReadableException(null, null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotReadableException(notReadableException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.FORMAT_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(notReadableException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleNotReadableException_ShouldTrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotReadableException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleNoHandlerFoundException(org.springframework.web.servlet.NoHandlerFoundException)}.
     */
    @Test
    public void testHandleNoHandlerFoundException()
    {
        ServletServerHttpRequest req = new ServletServerHttpRequest(new MockHttpServletRequest("GET", "/resource"));
        final NoHandlerFoundException noHandlerFoundException = new NoHandlerFoundException(req.getMethod().toString(), req.getServletRequest().getRequestURI(), req.getHeaders());
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNoHandlerFoundException(noHandlerFoundException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.URL_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(noHandlerFoundException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleNoHandlerFoundException_WithNull()
    {
        final NoHandlerFoundException noHandlerFoundException = new NoHandlerFoundException(null, null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNoHandlerFoundException(noHandlerFoundException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.URL_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(noHandlerFoundException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleNoHandlerFoundException_ShouldThrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNoHandlerFoundException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleConstraintViolationException(javax.validation.ConstraintViolationException)}.
     */
    @Test
    public void testHandleConstraintViolationException()
    {
        Set<ConstraintViolation<User>> constraintViolations = Sets.newHashSet();
        // constraintViolations.add(User);
        final ConstraintViolationException violationException = new ConstraintViolationException("Violation de contraintes", constraintViolations);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleConstraintViolationException(violationException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.CONTRAINST_VALDATION_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(violationException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleConstraintViolationException_WithNull()
    {
        final ConstraintViolationException violationException = new ConstraintViolationException(null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleConstraintViolationException(violationException);

        assertThat(response).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleConstraintViolationException_ShouldThrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleConstraintViolationException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleNotFoundException(java.lang.Exception)}.
     */
    @Test
    public void testHandleNotFoundException()
    {
        final EntityNotFoundException entityNotFoundException = new EntityNotFoundException("Entité non trouvée.");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotFoundException(entityNotFoundException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.NOT_FOUND_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(entityNotFoundException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleNotFoundException_WithNull()
    {
        final EntityNotFoundException entityNotFoundException = new EntityNotFoundException(null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotFoundException(entityNotFoundException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.NOT_FOUND_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(entityNotFoundException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleNotFoundException_ShouldThrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleNotFoundException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleDataIntegrityException(org.springframework.dao.DataIntegrityViolationException)}.
     */
    @Test
    public void testHandleDataIntegrityException()
    {
        final DataIntegrityViolationException dataIntegrityViolationException = new DataIntegrityViolationException("Violation intégrité des données.");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleDataIntegrityException(dataIntegrityViolationException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.INTEGRITY_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(dataIntegrityViolationException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleDataIntegrityException_WithNull()
    {
        final DataIntegrityViolationException dataIntegrityViolationException = new DataIntegrityViolationException(null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleDataIntegrityException(dataIntegrityViolationException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(response.getBody().getErrors().getDetails()).isEqualTo(AppConstants.INTEGRITY_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(dataIntegrityViolationException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleDataIntegrityException_ShouldThrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleDataIntegrityException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleMethodArgumentTypException(org.springframework.web.method.annotation.MethodArgumentTypeMismatchException)}.
     */
    @Test
    public void testHandleMethodArgumentTypException() throws NoSuchMethodException, SecurityException
    {
        Method method = User.class.getMethod("setUsername", String.class);
        MethodParameter methodParameter = new MethodParameter(method, 0);
        MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, User.class, "name", methodParameter, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleMethodArgumentTypException(mismatchException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.METHOD_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(mismatchException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleMethodArgumentTypException_()
    {
        MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleMethodArgumentTypException(mismatchException);

        assertThat(response).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleMethodArgumentTypException_ShouldThrowNPE()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleMethodArgumentTypException(null);

        assertThat(response).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.errors.AppExceptionHandler#handleAccessDeniedException(java.lang.Exception)}.
     */
    @Test
    public void testHandleAccessDeniedException()
    {
        AccessDeniedException accessDeniedException = new AccessDeniedException("Accès non autorisé");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleAccessDeniedException(accessDeniedException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.METHOD_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(accessDeniedException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleAccessDeniedException_Oauth2()
    {
        OAuth2AccessDeniedException oAuth2AccessDeniedException = new OAuth2AccessDeniedException("Accès non autorisé OAuth2");
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleAccessDeniedException(oAuth2AccessDeniedException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.METHOD_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(oAuth2AccessDeniedException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test
    public void testHandleAccessDeniedException_WithNull()
    {
        AccessDeniedException accessDeniedException = new AccessDeniedException(null, null);
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleAccessDeniedException(accessDeniedException);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getData()).isNull();
        assertThat(response.getBody().getErrors()).isNotNull();
        assertThat(response.getBody().getErrors()).isExactlyInstanceOf(ApiResponseError.class);
        assertThat(response.getBody().getErrors().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(response.getBody().getErrors().getDetails()).isNotEqualTo(AppConstants.METHOD_ERROR);
        assertThat(response.getBody().getErrors().getDebugMessage()).isEqualTo(accessDeniedException.getMessage());
        assertThat(response.getBody().getErrors().getValidationErrors()).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testHandleAccessDeniedException_ShouldThrowException()
    {
        final ResponseEntity<GenericApiResponse<User>> response = this.appExceptionHandler.handleAccessDeniedException(null);

        assertThat(response).isNull();
    }

}
