/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : UserMapperTest.java
 * Date de création : 9 déc. 2020
 * Heure de création : 20:16:26
 * Package : fr.vincent.tuto.server.service.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.server.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.ServerAppTestsUtil;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.config.db.PersistanceContextConfig;
import fr.vincent.tuto.server.enumeration.Role;
import fr.vincent.tuto.server.model.payload.UserDTO;
import fr.vincent.tuto.server.model.po.User;

/**
 * Classe des Tests Uitaires des objets de type {@link UserMapper}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "userMapperTest", classes = { AppRootConfig.class, DatabasePropsService.class, PersistanceContextConfig.class, UserMapper.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserMapperTest
{
    //
    private User client;
    private User admin;
    private UserDTO userDTO;

    @Autowired
    private UserMapper userMapper;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.userMapper.afterPropertiesSet();
        // ADMIN
        this.admin = ServerAppTestsUtil.createActiveUser(Role.ROLE_ADMIN.getAuthority(), "admin", "admin_19511982#", "admin.test@live.fr");

        // CLIENT
        this.client = ServerAppTestsUtil.createActiveUser(Role.ROLE_USER.getAuthority(), "client1", "client1_19511982#", "client1.test@live.fr");

        // DTO
        this.userDTO = ServerAppTestsUtil.createDTO();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.userMapper = null;
    }


    /**
     * Test method for
     * {@link fr.vincent.tuto.server.service.mapper.UserMapper#toDestObject(fr.vincent.tuto.server.model.po.User)}.
     */
    @Test
    public void testToDestObjectUser()
    {
        final UserDTO userDTO = this.userMapper.toDestObject(this.admin);

        assertThat(userDTO).isNotNull();
        assertThat(userDTO.getUsername()).isEqualTo("admin");
        assertThat(userDTO.getPassword()).contains("$12$");
        assertThat(userDTO.getEmail()).isEqualTo(this.admin.getEmail());
        assertThat(userDTO.getAccountExpired()).isEqualTo(this.admin.getAccountExpired());
        assertThat(userDTO.getAccountLocked()).isEqualTo(this.admin.getAccountLocked());
        assertThat(userDTO.getCredentialsExpired()).isEqualTo(this.admin.getCredentialsExpired());
        assertThat(userDTO.getEnabled()).isEqualTo(this.admin.getEnabled());
        assertThat(userDTO.getRoles()).isNotEqualTo(this.admin.getRoles());
        assertThat(userDTO.getCreatedTime()).isEqualTo(this.admin.getCreatedTime());
        assertThat(userDTO.getUpdatedTime()).isEqualTo(this.admin.getUpdatedTime());
    }

    @Test
    public void testToDestObjectUser_Client()
    {
        final UserDTO userDTO = this.userMapper.toDestObject(this.client);

        assertThat(userDTO).isNotNull();
        assertThat(userDTO.getUsername()).isEqualTo("client1");
        assertThat(userDTO.getPassword()).contains("$12$");
        assertThat(userDTO.getEmail()).isEqualTo(this.client.getEmail());

        assertThat(userDTO.getAccountExpired()).isEqualTo(this.client.getAccountExpired());
        assertThat(userDTO.getAccountLocked()).isEqualTo(this.client.getAccountLocked());
        assertThat(userDTO.getCredentialsExpired()).isEqualTo(this.client.getCredentialsExpired());
        assertThat(userDTO.getEnabled()).isEqualTo(this.client.getEnabled());
        assertThat(userDTO.getRoles()).isNotEqualTo(this.client.getRoles());
        assertThat(userDTO.getCreatedTime()).isEqualTo(this.client.getCreatedTime());
        assertThat(userDTO.getUpdatedTime()).isEqualTo(this.client.getUpdatedTime());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToDestObjectUser_ShouldhrowException()
    {
        final UserDTO userDTO = this.userMapper.toDestObject(null);

        assertThat(userDTO).isNull();
    }

    public void testToDestObjectUser_ShouldhrowExceptionJunit5()
    {
        final Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            this.userMapper.toDestObject(null);
        });

        // System.err.println(">>>>>>>> Message = \n" + exception.getMessage());
        // System.err.println(">>>>>>>> Message2 = \n" + exception.getLocalizedMessage());

        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isNotEmpty();
        assertThat(exception.getMessage()).isEqualTo("source cannot be null");
        assertThat(exception.getLocalizedMessage()).isEqualTo("source cannot be null");
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.service.mapper.UserMapper#toSourceObject(fr.vincent.tuto.server.model.payload.UserDTO)}.
     */
    @Test
    public void testToSourceObjectUserDTO()
    {
        final User user = this.userMapper.toSourceObject(this.userDTO);

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo("dtoUser");
        assertThat(user.getPassword()).contains("$12$");

        assertThat(user.getRoles()).isNotNull();
        assertThat(user.getRoles().size()).isEqualTo(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToSourceObjectUserDTO_ShouldThrownException()
    {
        final User user = this.userMapper.toSourceObject(null);
        assertThat(user).isNull();
    }


    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericObjectMapper#toDestObjectList(java.util.Collection)}.
     */
    @Test
    public void testToDestObjectList()
    {
        List<User> users = Lists.newArrayList();
        users.add(client);
        users.add(admin);
        users.add(null);

        final List<UserDTO> userDTOs = (List<UserDTO>) this.userMapper.toDestObjectList(users);

        assertThat(userDTOs).isNotNull();
        assertThat(userDTOs).isNotEmpty();
        assertThat(userDTOs.size()).isEqualTo(2);
        assertThat(userDTOs.get(0).getUsername()).isEqualTo(this.client.getUsername());
        assertThat(userDTOs.get(1).getUsername()).isEqualTo(this.admin.getUsername());
    }

    @Test
    public void testToDestObjectList_WithNullList()
    {
        final List<UserDTO> userDTOs = (List<UserDTO>) this.userMapper.toDestObjectList(null);

        assertThat(userDTOs).isNotNull();
        assertThat(userDTOs).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericObjectMapper#toSourceObjectList(java.util.Collection)}.
     */
    @Test
    public void testToSourceObjectList()
    {
        List<UserDTO> dtos = Lists.newArrayList();
        dtos.add(this.userDTO);
        dtos.add(this.userDTO);
        dtos.add(this.userDTO);
        dtos.add(null);

        final List<User> users = (List<User>) this.userMapper.toSourceObjectList(dtos);

        assertThat(users).isNotNull();
        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(3);
        assertThat(users.get(0).getUsername()).isEqualTo(this.userDTO.getUsername());
        assertThat(users.get(0).getPassword()).isNotEqualTo(this.userDTO.getPassword());// Car le générateur des jeux de tests encode déjà le mot de passe
    }

    @Test
    public void testToSourceObjectList_WithNullList()
    {
        final List<User> users = (List<User>) this.userMapper.toSourceObjectList(null);

        assertThat(users).isNotNull();
        assertThat(users).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.service.mapper.UserMapper#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.userMapper.afterPropertiesSet();
    }
}
