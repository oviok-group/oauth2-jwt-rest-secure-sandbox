/*
 * ----------------------------------------------
 * Projet ou Module : oauth2-jwt-rest-secure-server
 * Nom de la classe : CustomUserDetailsServiceTest.java
 * Date de création : 24 déc. 2020
 * Heure de création : 07:04:06
 * Package : fr.vincent.tuto.server.service.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.server.service.security;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.service.props.DatabasePropsService;
import fr.vincent.tuto.server.ServerAppTestsUtil;
import fr.vincent.tuto.server.config.AppRootConfig;
import fr.vincent.tuto.server.config.db.PersistanceContextConfig;
import fr.vincent.tuto.server.constant.ServerConstants;

/**
 * Classe des Tests unitaires des objets de type {@link CustomUserDetailsService}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:oauth2-jwt-rest-secure-application-test.properties", "classpath:oauth2-jwt-rest-secure-db-test.properties" })
@ContextConfiguration(name = "customUserDetailsServiceTest", classes = { AppRootConfig.class, DatabasePropsService.class,
        PersistanceContextConfig.class, CustomUserDetailsService.class })
@SpringBootTest
@ActiveProfiles("test")
@Sql(scripts = { "classpath:db/h2/schema-test-h2.sql", "classpath:db/h2/data-test-h2.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
public class CustomUserDetailsServiceIT
{
    //
    @Autowired
    private CustomUserDetailsService userDetailsService;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.server.service.security.CustomUserDetailsService#loadUserByUsername(java.lang.String)}.
     */
    @Test
    public void testLoadUserByUsername()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.USER_ADMIN_UPPER);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(ServerAppTestsUtil.USER_ADMIN_USERNAME);
        assertThat(userDetails.getPassword()).contains(ServerAppTestsUtil.PWD_PATTERN);
        assertThat(userDetails.getAuthorities()).isNotEmpty();

        @SuppressWarnings("unchecked")
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
        assertThat(authorities).isNotEmpty();
        assertThat(authorities.size()).isEqualTo(2);
        assertThat(ServerConstants.ROLES).containsAll(authorities);
    }

    @Test
    public void testLoadUserByUsername_Lower()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.USER_ADMIN_USERNAME);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(ServerAppTestsUtil.USER_ADMIN_USERNAME);
        assertThat(userDetails.getPassword()).contains(ServerAppTestsUtil.PWD_PATTERN);
        assertThat(userDetails.getAuthorities()).isNotEmpty();

        @SuppressWarnings("unchecked")
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
        assertThat(authorities).isNotEmpty();
        assertThat(authorities.size()).isEqualTo(2);
        assertThat(ServerConstants.ROLES).containsAll(authorities);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsername_Empty()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(StringUtils.EMPTY);

        assertThat(userDetails).isNull();
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsername_BadLoginShouldThrowException()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.USER_ONE_USERNAME);

        assertThat(userDetails).isNull();
    }

    @Test
    public void testLoadUserByUsername_WithMail()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.ADMIN_EMAIL);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(ServerAppTestsUtil.USER_ADMIN_USERNAME);
        assertThat(userDetails.getPassword()).contains(ServerAppTestsUtil.PWD_PATTERN);
        assertThat(userDetails.getAuthorities()).isNotEmpty();

        @SuppressWarnings("unchecked")
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
        assertThat(authorities).isNotEmpty();
        assertThat(authorities.size()).isEqualTo(2);
        assertThat(ServerConstants.ROLES).containsAll(authorities);
    }

    @Test
    public void testLoadUserByUsername_WithMailLower()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.ADMIN_EMAIL_LOWER);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(ServerAppTestsUtil.USER_ADMIN_USERNAME);
        assertThat(userDetails.getPassword()).contains(ServerAppTestsUtil.PWD_PATTERN);
        assertThat(userDetails.getAuthorities()).isNotEmpty();

        @SuppressWarnings("unchecked")
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
        assertThat(authorities).isNotEmpty();
        assertThat(authorities.size()).isEqualTo(2);
        assertThat(ServerConstants.ROLES).containsAll(authorities);
    }

    @Test(expected = CustomAppException.class)
    public void testLoadUserByUsername_WithInactiveUser()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(ServerAppTestsUtil.INACTIVE_USER_MAIL);

        assertThat(userDetails).isNull();
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsername_WithNull()
    {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(null);

        assertThat(userDetails).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.server.service.security.CustomUserDetailsService#getUserDAO()}.
     */
    @Test
    public void testGetUserDAO()
    {
        assertThat(this.userDetailsService.getUserDAO()).isNotNull();
    }

}
